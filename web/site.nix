{ mkDerivation, base, filepath, hakyll, stdenv }:
mkDerivation {
  pname = "site";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base filepath hakyll ];
  homepage = "https://splintah.gitlab.io/pws";
  license = stdenv.lib.licenses.agpl3;
}
