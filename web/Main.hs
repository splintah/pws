{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified GHC.IO.Encoding as E
import           Hakyll
import           System.FilePath (replaceExtension)

main :: IO ()
main = do
  E.setLocaleEncoding E.utf8
  hakyll $ do
    match "index.org" $ do
      route $ setExtension "html"
      compile $ pandocCompiler
        >>= loadAndApplyTemplate "templates/default.html" defaultContext
        >>= relativizeUrls

    match "artikelen/*" $ do
      route $ customRoute $
        (`replaceExtension` "html") .
        (replaceAll "artikelen/" (const "")) .
        toFilePath
      compile $ pandocCompiler
        >>= loadAndApplyTemplate "templates/artikel.html" defaultContext
        >>= relativizeUrls

    match "bestanden/*" $ do
      route idRoute
      compile copyFileCompiler

    match "css/*" $ do
      route idRoute
      compile compressCssCompiler

    match "templates/*" $ compile templateBodyCompiler
