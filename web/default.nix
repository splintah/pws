{ nixpkgs ? import <nixpkgs> {}, compiler ? "ghc865" }:
nixpkgs.haskell.packages.${compiler}.callPackage ./site.nix { }
