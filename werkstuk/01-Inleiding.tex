\section{Inleiding}
% Zeggen dat dit voor leken is en dat de hoofdstukken later uitgebreider worden besproken.
In dit hoofdstuk wordt een inleiding gegeven tot dit onderzoek, die zich richt op medeleerlingen.
De rest van het onderzoek is lastiger te lezen, omdat er veel achtergrondkennis voor nodig is waarvoor de bronnen gelezen moeten worden.
Later zal een uitgebreidere hoofdstukindeling worden gegeven.

\subsection{Achtergrond}
Om een computer een taak uit te laten voeren, moet een programmeur een programma schrijven dat opdrachten geeft aan de computer.
Zo'n programma wordt geschreven in een programmeertaal, een kunstmatige, vaak redelijk simpele taal.
Een voorbeeld is een programma dat de faculteit van een getal berekent.
Je zou de definitie van de faculteit kunnen opschrijven als:
\[
  n! = n \times (n - 1) \times (n - 2) \times \cdots \times 3 \times 2 \times 1\text{.}
\]
(\(n!\) betekent de faculteit van \(n\).)
De faculteit van \(6\) is bijvoorbeeld:
\[
  6! = 6 \times 5 \times 4 \times 3 \times 2 \times 1 = 720
\]
Een computer kan de definitie hierboven niet begrijpen, omdat deze te vaag is voor een computer, dus we moeten het voor hem wat makkelijker maken.
Merk op dat de faculteit van \(n\) gelijk is aan \(n\) keer de faculteit van \(n - 1\), dus \(n! = n \times (n - 1)!\);
bij \(1\) stoppen we dan, want \(1! = 1\).
In de programmeertaal Python kan deze functie zo geschreven worden:
\begin{lstlisting}
def faculteit(n):
  if n <= 1:
    return 1
  else:
    return n * faculteit(n - 1)
\end{lstlisting}
Dit programma leest bijna als een Engelse zin: definieer (\texttt{def}) een functie genaamd \(\mathit{faculteit}\) voor een getal \(n\) (\texttt{faculteit(n)}).
Als \(n <= 1\) (\texttt{if n <= 1:}), geef dan \(1\) terug als antwoord (\texttt{return 1}).
Anders (\texttt{else:}), geef \(n \times (n - 1)!\) terug als antwoord (\texttt{return n * faculteit(n - 1)}).

Om het begrijpen van een programma makkelijker te maken voor de computer, gebruiken we een simpele taal zoals Python, veel simpeler dan bijvoorbeeld Nederlands of Engels.
Voor de mens zijn programma's geschreven in een programmeertaal vaak minder makkelijk te begrijpen dan teksten in een natuurlijke taal.
Daarom worden programmeertalen vaak zo gemaakt dat ze nog redelijk op (in de meeste gevallen) Engels lijken.

De computer kan met zo'n programma nog niet aan het werk.
Er moet eerst een vertaalslag plaatsvinden, voordat de computer het programma kan uitvoeren.
Er zijn grofweg twee manieren om een programma uit te voeren: een \emph{interpretator} kan het programma \emph{interpreteren} en een \emph{compiler} kan het programma \emph{compileren} (vertalen).
Interpretatoren en compilers en hun voor- en nadelen worden in het vervolg besproken.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={rectangle, draw=black, fill=white, thick, minimum size=5mm},
  ]
    \node[knoop] (invoer) {Invoer: programma in programmeertaal};
    \node[knoop] (syntaxisboom) [below=of invoer] {Boom};
    \node[knoop] (binary) [below right=of syntaxisboom] {Machinetaal};
    \node        (ruimte) [below=of syntaxisboom] {};
    \node[knoop] (resultaat) [below=of ruimte] {Resultaat};

    \draw[->] (invoer.south) -- (syntaxisboom.north) node[left, midway] {Ontleden};
    \draw[->] (syntaxisboom.south) -- (resultaat.north) node[left, midway] {Interpreteren};
    \draw[->] (syntaxisboom.-45) -- (binary.north west) node[above right, midway] {Compileren};
    \draw[->] (binary.south west) -- (resultaat.45) node[below right, midway] {Uitvoeren};
  \end{tikzpicture}
  \caption{Uitvoerschema van een programma}
  \label{fig:uitvoerschema}
\end{figure}

In figuur \ref{fig:uitvoerschema} is schematisch weergegeven hoe een programma uitgevoerd wordt.
De invoer, het programma in tekstvorm, wordt eerst ontleed.
Er wordt dan een zogeheten boom gemaakt waarmee de computer beter kan rekenen dan met de invoer die bestaat uit een lijst van tekens.
In figuur \ref{fig:ast} is een voorbeeld van zo'n boom weergegeven voor het programma \texttt{2 + 3 * 5}.
(Wiskundigen en informatici tekenen bomen altijd ondersteboven, met takken die naar beneden groeien.)

\subsubsection*{Ontleden}
Een ontleder, meestal \emph{parser} genoemd, moet rekening houden met de voorrang van wiskundige bewerkingen, want \texttt{(2 + 3) * 5} is niet hetzelfde als \texttt{2 + (3 * 5)}.
Een computer kan beter met een boom rekenen, omdat een boom meer structuur heeft dan de tekstinvoer.
In het tekstvoorbeeld staan de \texttt{2} en de \texttt{5} even dicht bij de \texttt{3}, terwijl \texttt{*} `harder trekt' aan de getallen die eromheen staan dan \texttt{+}.
In de boom is de afstand (over de lijnen) tussen de \texttt{2} en de \texttt{3} daarom groter dan die tussen de \texttt{5} en de \texttt{3}.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
  ]
    \node[knoop] (optelling) {\texttt{+}};
    \node[knoop] (twee) [below left=of optelling] {\texttt{2}};
    \node[knoop] (vermenigvuldiging) [below right=of optelling] {\texttt{*}};
    \node[knoop] (drie) [below left=of vermenigvuldiging] {\texttt{3}};
    \node[knoop] (vijf) [below right=of vermenigvuldiging] {\texttt{5}};

    \draw[-] (optelling.south west) -- (twee.north east);
    \draw[-] (optelling.south east) -- (vermenigvuldiging.north west);
    \draw[-] (vermenigvuldiging.south west) -- (drie.north east);
    \draw[-] (vermenigvuldiging.south east) -- (vijf.north west);
  \end{tikzpicture}
  \caption{Boom van \texttt{2 + 3 * 5}}
  \label{fig:ast}
\end{figure}

\subsubsection*{Interpreteren}
Nadat de invoer is ontleed en een boom is gemaakt, is het proces van een interpretator en een compiler verschillend.
Een interpretator voert het programma dat is opgeslagen in de boom meteen uit.
De interpretator zou bij het interpreteren van het voorbeeld \texttt{2 + 3 * 5} eerst een \texttt{+} tegenkomen, die bovenaan de boom staat.
De interpretator voert dan het linker- en rechterdeel uit, en telt de uitkomsten bij elkaar op.
Voor het linkerdeel hoeft niets gedaan te worden: het resultaat is gewoon \texttt{2}.
Voor het rechterdeel moet wel iets gedaan worden: de interpretator komt een \texttt{*} met twee vertakkingen tegen.
Om het resultaat te bepalen, worden, net zoals bij \texttt{+} het linkerdeel en het rechterdeel van de \texttt{*} uitgevoerd, maar dit keer worden ze vermenigvuldigd met elkaar.
Het linkerdeel van de \texttt{*} heeft als resultaat \texttt{3}, het rechterdeel \texttt{5}.
Het resultaat van het rechterdeel van de \texttt{+} is dus \(3 \times 5 = 15\).
Het resultaat van het gehele programma is daarom \(2 + 15 = 17\).

De stappen die de interpretator heeft genomen kunnen we zo weergeven:
\begin{lstlisting}
2 + 3 * 5
2 + 15
17
\end{lstlisting}
Voor het berekenen van de faculteit moet een interpretator meer stappen nemen;
voor \texttt{faculteit(6)} zien de stappen er zo uit:
\begin{lstlisting}
faculteit(6)
6 * faculteit(5)
6 * 5 * faculteit(4)
6 * 5 * 4 * faculteit(3)
6 * 5 * 4 * 3 * faculteit(2)
6 * 5 * 4 * 3 * 2 * faculteit(1)
6 * 5 * 4 * 3 * 2 * 1
6 * 5 * 4 * 3 * 2
6 * 5 * 4 * 6
6 * 5 * 24
6 * 120
720
\end{lstlisting}

Een interpretator werkt door middel van \emph{substitutie}, vervanging.
Om \texttt{faculteit(6)} uit te rekenen, wordt elke \texttt{n} in de definitie van \texttt{faculteit(n)} vervangen door 6.
In figuur \ref{fig:ast-substitutie} is de definitie van \texttt{faculteit(n)} weergegeven.
Zo'n substitutie kost veel tijd, omdat de hele boom doorlopen moet worden om elke \texttt{n} te vinden en te vervangen.
Bij het interpreteren van \texttt{faculteit(6)} wordt ook \texttt{faculteit(5)} (rechtsonder in de figuur) berekend.
Dan moet de boom van de onderstaande figuur gekopieerd worden, en wordt elke \texttt{n} vervangen door \texttt{5}.
Dit herhaalt zich tot we zijn aangekomen bij \texttt{faculteit(1)}.
Door het kopiëren kost interpreteren vaak veel geheugen.
Daarnaast is de substitutie een tijdrovend proces, zeker als het vaak moet gebeuren.
\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
     cirkel/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
     rechthoek/.style={rectangle, draw=black, fill=white, thick, minimum size=5mm, rounded corners},
     gekleurd/.style={fill=black!35},
     node distance=7mm,
  ]
    \node[rechthoek] (if-then-else) {\texttt{if} \ldots\texttt{:} \ldots{} \texttt{else:} \ldots};
      \node[cirkel] (le) [below left=of if-then-else] {\texttt{<=}};
      \draw[-] (if-then-else.south west) -- (le.north east);
        \node[cirkel,gekleurd] (n-le) [below left=of le] {\texttt{n}};
        \draw[-] (le.south west) -- (n-le.north east);
        \node[cirkel] (le-een) [below right=of le] {\texttt{1}};
        \draw[-] (le.south east) -- (le-een.north west);
      \node[rechthoek] (return-een) [below=of if-then-else] {\texttt{return} \ldots};
      \draw[-] (if-then-else.south) -- (return-een.north);
        \node[cirkel] (een-return) [below=of return-een] {\texttt{1}};
        \draw[-] (return-een.south) -- (een-return.north);
      \node[rechthoek] (return-fac) [below right=of if-then-else] {\texttt{return} \ldots};
      \draw[-] (if-then-else.south east) -- (return-fac.north west);
        \node[cirkel] (verm) [below=of return-fac] {\texttt{*}};
        \draw[-] (return-fac.south) -- (verm.north);
          \node[cirkel,gekleurd] (n-keer) [below left=of verm] {\texttt{n}};
          \draw[-] (verm.south west) -- (n-keer.north east);
          \node[rechthoek] (fac-n-1) [below right=of verm] {\texttt{faculteit(}\ldots\texttt{)}};
          \draw[-] (verm.south east) -- (fac-n-1.north west);
            \node[cirkel] (min) [below=of fac-n-1] {\texttt{-}};
            \draw[-] (fac-n-1.south) -- (min.north);
              \node[cirkel,gekleurd] (n-min) [below left=of min] {\texttt{n}};
              \draw[-] (min.south west) -- (n-min.north east);
              \node[cirkel] (min-een) [below right=of min] {\texttt{1}};
              \draw[-] (min.south east) -- (min-een.north west);
  \end{tikzpicture}
  \caption{De boom van de definitie van \texttt{faculteit(n)}.}
  Voor het berekenen van \texttt{faculteit(6)} moet elke \texttt{n} in deze boom vervangen worden door een \texttt{6}.
  \label{fig:ast-substitutie}
\end{figure}
% Om \texttt{6 * faculteit(5)} uit te rekenen, wordt de waarde van \texttt{5 * faculteit(4)} bepaald, en wordt \texttt{faculteit(5)} vervangen door die waarde.
% Het grote probleem voor interpretatoren is dat substitutie veel tijd en geheugen kost.
% De term \texttt{faculteit(6)} wordt vervangen door de term \texttt{6 * 5 * 4 * 3 * 2 * 1}.
% In figuur \ref{fig:ast-vergelijking} is weergegeven hoe de boom van de term \texttt{faculteit(6)} groeit tot de boom van \texttt{6 * 5 * 4 * 3 * 2 * 1}.
% Zoals in de figuur te zien is, kost het veel ruimte om de boom van een grote som op te slaan.
% Interpretatoren zijn daardoor meestal trager dan programma's gemaakt door compilers.

% \begin{figure}[H]
%   \centering
%   \begin{tikzpicture}[
%     cirkel/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
%     rechthoek/.style={rectangle, draw=black, fill=white, thick, minimum size=5mm, rounded corners},
%   ]
%     \node[rechthoek] (aanroep) {\texttt{functie (\ldots)}};
%     \node[rechthoek] (functie) [below left=of aanroep] {\texttt{faculteit}};
%     \node[cirkel] (zes) [below right=of aanroep] {\texttt{6}};

%     \draw[-] (aanroep.south west) -- (functie.north east);
%     \draw[-] (aanroep.south east) -- (zes.north west);
%   \end{tikzpicture}

%   \(\downarrow\)

%   \begin{tikzpicture}[
%     knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
%   ]
%     \node[knoop] (verm65) {\texttt{*}};
%     \node[knoop] (zes) [below left=of verm65] {\texttt{6}};
%     \node[knoop] (verm54) [below right=of verm65] {\texttt{*}};
%     \node[knoop] (vijf) [below left=of verm54] {\texttt{5}};
%     \node[knoop] (verm43) [below right=of verm54] {\texttt{*}};
%     \node[knoop] (vier) [below left=of verm43] {\texttt{4}};
%     \node[knoop] (verm32) [below right=of verm43] {\texttt{*}};
%     \node[knoop] (drie) [below left=of verm32] {\texttt{3}};
%     \node[knoop] (verm21) [below right=of verm32] {\texttt{*}};
%     \node[knoop] (twee) [below left=of verm21] {\texttt{2}};
%     \node[knoop] (een) [below right=of verm21] {\texttt{1}};

%     \draw[-] (verm65.south west) -- (zes.north east);
%     \draw[-] (verm65.south east) -- (verm54.north west);
%     \draw[-] (verm54.south west) -- (vijf.north east);
%     \draw[-] (verm54.south east) -- (verm43.north west);
%     \draw[-] (verm43.south west) -- (vier.north east);
%     \draw[-] (verm43.south east) -- (verm32.north west);
%     \draw[-] (verm32.south west) -- (drie.north east);
%     \draw[-] (verm32.south east) -- (verm21.north west);
%     \draw[-] (verm21.south west) -- (twee.north east);
%     \draw[-] (verm21.south east) -- (een.north west);
%   \end{tikzpicture}
%   \caption{Vergelijking van de bomen van \texttt{faculteit(6)} en \texttt{6 * 5 * 4 * 3 * 2 * 1}}
%   \label{fig:ast-vergelijking}
% \end{figure}

\subsubsection*{Compileren}
Een compiler vertaalt de boom naar een taal die de computer nog makkelijker kan begrijpen, zoals \emph{machinetaal}.
Deze taal lijkt helemaal niet meer op Nederlands of Engels, en zelfs niet op Python of de meeste andere programmeertalen.
Programma's in machinetaal bestaan uit de `enen en nullen' van de computer.
Als de computer nu het programma uit gaat voeren, hoeft hij alleen de enen en nullen uit te voeren, zonder dat de invoer ontleed moet worden en de vertaalstappen doorlopen worden.
Het uitvoeren van enen en nullen is erg snel, omdat de computer deze direct uit kan voeren.
Dat is het enige wat een computer echt `begrijpt'.

\subsubsection*{Tolk of vertaler}
Interpretatoren en compilers kunnen vergeleken worden met tolken en vertalers.
Een tolk vertaalt gesproken taal direct naar een andere taal.
Dat gebeurt direct, zodat twee mensen die elkaars taal niet spreken elkaar wel snel kunnen verstaan en met elkaar kunnen communiceren.
Een vertaler van geschreven teksten heeft vaak langer de tijd om een eindresultaat te maken.
Daardoor kan de vertaler meer aandacht besteden aan nuances.
Omdat een tolk snel met een vertaling moet komen, zijn de woordkeuze en grammaticale structuur vaak wat minder fijn dan die van een vertaler.
Het is ook geen toeval dat het woord interpretator, overgenomen van het Engelse \emph{interpreter}, een synoniem is van tolk.
Compileren, ook overgenomen uit het Engels, betekent letterlijk \emph{samenstellen}.
Dat lijkt aan te geven dat degene die compileert de tijd neemt om een volledig en weloverwogen resultaat te maken.

\subsubsection*{Voor- en nadelen}
Met een gecompileerde taal kunnen snellere programma's gemaakt worden dan met een geïnterpreteerde taal.
Talen met een compiler zijn dus vaak sneller dan interpretatoren als ze hetzelfde berekenen.
Daarnaast kan een compiler tijdens het vertalen meer tijd gebruiken om analyses uit te voeren die fouten kunnen ontdekken in het programma.
Als de compiler fouten van de programmeur kan ontdekken voordat het programma uitgevoerd wordt, is de kans dat het programma crasht bij het uitvoeren kleiner.
Voor een interpretator is daar geen tijd voor, omdat deze analyses telkens gedaan worden wanneer het programma uitgevoerd wordt.

Een voordeel van talen met een interpretator is echter, dat het programmeren vaak sneller en makkelijker is.
Het uitvoeren van een gecompileerd programma is wel sneller, maar het duurt vaak langer om een programma te compileren, dan om de interpretator op te starten.
Geïnterpreteerde talen maken het vaak makkelijker voor de programmeur om een deel van het programma uit te proberen.
Een ander voordeel van interpretatoren is dat ze vaak makkelijker te maken zijn dan compilers, bijvoorbeeld omdat machinetaal kan verschillen tussen computers:
een goede compiler moet voor zoveel mogelijk computers machinetaal kunnen produceren, zodat iedereen het programma op zijn/haar computer kan uitvoeren.
Maar dat kost veel tijd en moeite voor de makers van de compiler: elke machinetaal werkt net iets anders.

\subsubsection*{Tussentaal}
Voor dat laatste probleem probleem is een oplossing bedacht, die gebruikmaakt van een compiler én een interpretator.
Een compiler hoeft namelijk niet naar machinetaal te vertalen, maar kan ook naar een andere taal vertalen.
De makers van programmeertalen als Java en C\# hebben daarom nog een taal ontworpen, die ergens tussen de programmeertaal en de machinetaal in zit, een soort \emph{tussentaal}.
Deze taal kan niet direct door de computer uitgevoerd worden, en wordt daarom geïnterpreteerd.
Omdat deze taal erg simpel is en redelijk op machinetaal lijkt, wordt er minder aan snelheid verloren dan wanneer het programma zonder vertaalslag geïnterpreteerd zou worden.
In het schema van figuur \ref{fig:uitvoerschema} komt er dan een derde route bij, die in figuur \ref{fig:uitvoerschema-combinatie} is weergegeven.
Deze oplossing hebben wij bestudeerd in dit onderzoek.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={rectangle, draw=black, fill=white, thick, minimum size=5mm},
  ]
    \node[knoop] (invoer) {Invoer: programma in programmeertaal};
    \node[knoop] (syntaxisboom) [below=of invoer] {Boom};
    \node[knoop] (bytecode) [below=of syntaxisboom] {Taal tussen programmeertaal en machinetaal};
    \node[knoop] (resultaat) [below=of bytecode] {Resultaat};

    \draw[->] (invoer.south) -- (syntaxisboom.north) node[left, midway] {Ontleden};
    \draw[->] (syntaxisboom.south) -- (bytecode.north) node[left, midway] {Compileren};
    \draw[->] (bytecode.south) -- (resultaat.north) node[left, midway] {Interpreteren};
  \end{tikzpicture}
  \caption{Uitvoerschema van een programma met een combinatie van compileren en interpreteren}
  \label{fig:uitvoerschema-combinatie}
\end{figure}

\subsection{Ons onderzoek}
Wij hebben een eigen taal ontworpen die we SSML hebben genoemd.
Deze naam staat voor \textbf{S}ybrand en \textbf{S}plinters \textbf{ML}.
ML is een andere programmeertaal, waarop SSML is gebaseerd.
We hebben zelf een taal ontworpen, zodat we deze simpel konden houden.
De meeste programmeertalen zijn namelijk erg uitgebreid, om het programmeren makkelijker te maken.
Door de taal simpel te houden hebben we de omvang van het project proberen te beperken zodat deze goed aansluit bij de omvang van een profielwerkstuk.
Voor SSML hebben we een compiler gemaakt die programma's vertaalt naar een simpelere en minder abstracte taal.
Het grootste aandachtspunt van dit onderzoek was een compiler die mogelijke programmeerfouten uit programma's kan halen voordat ze uitgevoerd worden.
Ook zijn de programma's in theorie sneller, maar dat is niet uitgebreid getest.

Het programma dat de faculteit van \(6\) berekent ziet er zo uit in SSML:
\begin{lstlisting}
main =
  let rec faculteit = \n ->
    if n <= 1
      then 1
      else n * faculteit (n - 1)
  in
  faculteit 6;
\end{lstlisting}
Dit programma werkt hetzelfde als het Python-programma.

Het bovenstaande programma wordt door de compiler vertaald naar het volgende programma in de tussentaal:
\begin{lstlisting}
SI(K(6))(Y(S(K(S(S(S(S(Kle)I)(K(1)))(K(1)))))
(S(K(S(S(Kmul)I)))(S(S(KS)(S(KK)I))(K(S(S(Ksub)I)
(K(1))))))))
\end{lstlisting}
Zoals je ziet, is dit voor mensen nauwelijks te begrijpen.
Enkele delen kunnen we wel herkennen: zo zien we de getallen \(6\) en \(1\) terug en herkennen we de functies \(\skifn{le}\) (\emph{less than or equal}, kleiner dan of gelijk aan), \(\skifn{mul}\) (\emph{multiplication}, vermenigvuldiging) en \(\skifn{sub}\) (\emph{subtraction}, aftrekking).
Dit programma wordt daarna geïnterpreteerd.

\subsection{Motivatie}
Met dit onderzoek hebben wij geprobeerd de werking van een programmeertaal te begrijpen: hoe een taal ontleed, vertaald en uitgevoerd wordt, en hoe analyses toegepast kunnen worden om de taal veiliger te maken.
Hiervoor hebben wij een eigen compiler en interpretator gemaakt.

\subsection{Bronnen}
Het onderzoek is met name gebaseerd op \cite{Pierce02} voor de compiler en \cite{Turner79} voor de interpretator.
In het vervolg wordt de werking van de compiler en de interpretator uitgelegd.
Voor beiden is ook een concrete implementatie gemaakt, beschikbaar via \url{https://gitlab.com/splintah/pws}.

\subsection{Hoofdstukindeling}
In \S\ref{sec:theoretische-inleiding} wordt de (wiskundige) achtergrond van dit onderzoek uitvoerig beschreven, zodat de lezer een minder grote voorkennis van het onderwerp nodig heeft.
In \S\ref{sec:ssml} wordt de taal SSML beschreven, voortbouwend op de theoretische inleiding: hoe de taal ontleed wordt en hoe de analyses werken.
In \S\ref{sec:ski} wordt de interpretator van de taal die zich tussen SSML en machinetaal in bevindt besproken.
In \S\ref{sec:ssml-naar-ski} wordt de werking van de compiler beschreven.
Verder sluiten we in \S\ref{sec:conclusie} af met onze conclusie en geven we in \S\ref{sec:discussie} aan waar vervolgonderzoek zich op kan richten.
