\section{Theoretische inleiding}\label{sec:theoretische-inleiding}

Het paradigma van functionele programmeertalen heeft zijn oorsprong in de \lamcalc{}.
Om SSML, een functionele taal, en de SKI-combinatorcalculus, een codering van de \lamcalc{}, te begrijpen, is daarom kennis van de \lamcalc{} nodig.
De ongetypeerde, de simpel getypeerde en de polymorfe \lamcalc{} zullen in dit hoofdstuk besproken worden.
Daarnaast zal de SKI-combinatorcalculus en het verband met de ongetypeerde \lamcalc{} worden besproken.

\subsection{De ongetypeerde lambda-calculus}
De (ongetypeerde) \emph{\lamcalc{}} is een formeel systeem ontwikkeld door Alonzo Church, om onderzoek te doen naar beslisbaarheid en berekenbaarheid.
De \lamcalc{} kan ook gebruikt worden als programmeertaal, en is het fundament van het paradigma van \emph{functionele} programmeertalen.
Zie voor een uitgebreide beschrijving \cite{Barendregt00}.

\subsubsection{Syntaxis}
De \lamcalc{} heeft drie verschillende soorten termen:
\begin{align*}
  t ::=\ & x             & \text{(variabele)} \\
      |\ & \lamAbs{x}{t} & \text{(abstractie)} \\
      |\ & \lamApp{t}{t} & \text{(applicatie)}
\end{align*}
Hiervoor geldt dat \(x\) voor een (willekeurige) variabele staat.
Voor de namen van variabelen worden vaak (Latijnse) letters of woorden gebruikt.

Deze regels beschrijven geen volledige context-vrije taal (\cite[hoofdstuk 2]{Jeuring17}; \cite[\S 5.2]{Silva14}), maar beschrijven wel de \emph{soorten} termen die er zijn, zodat functies gedefinieerd kunnen worden die onderscheid maken tussen termen.
Het is dus een abstracte syntaxis, en geen concrete syntaxis;
dat betekent bijvoorbeeld dat in de syntaxisregels de voorrang van termen en daarmee het gebruik van haakjes impliciet worden gelaten.
In het vervolg wordt ook voor elke taal een abstracte syntaxis gegeven.

\(\lambda\)-abstracties kunnen gezien worden als functies;
de term \(\lamAbs{x}{t}\) is dan de functie \(x |-> t\), waar \(t\) een term is die afhankelijk is van \(x\).
\(\lambda\)-abstracties in de vorm \(\lamAbs{x}{t}\) \emph{binden} de variabele \(x\);
telkens wanneer \(x\) voorkomt in \(t\) is \(x\) \emph{gebonden}.
De niet-gebonden variabelen in een term noemt men de \emph{vrije variabelen} (Engels: \emph{free variables}).
De verzameling van vrije variabelen in een term wordt gegeven door:
\begin{align*}
  FV(x) & = \{x\} \\
  FV(\lamAbs{x}{t}) & = FV(t) \setminus \{x\} \\
  FV(\lamApp{t_1}{t_2}) & = FV(t_1) \cup FV(t_2)
\end{align*}

De applicatie van termen kan vergeleken worden met het toepassen van afbeeldingen op argumenten.
Om de semantiek van applicaties te beschrijven, moeten eerst \emph{substituties} bestudeerd worden.
Een substitutie vervangt de variabelen in een term door andere termen.
De substitutie waarin alle (vrije) variabelen \(x\) in de term \(t\) vervangen worden door de term \(s\) wordt geschreven als \([x := s] t\).
Substitutie wordt als volgt gedefinieerd (\cite[\S 5.3]{Pierce02}):
\begin{equation*}
\begin{array}{l l r}
  %% NOOT: de blokhaken moeten binnen accolades, omdat ze anders door LaTeX
  %% worden gezien als parameters.
  {[x := s]}x & = s & \\
  {[x := s]}y & = y, & \text{als } x \neq y \\
  {[x := s]}(\lamAbs{y}{t}) & = \lamAbs{y}{[x := s]t}, & \text{als } x \neq y \text{ en } y \not\in FV(s) \\
  {[x := s]}(\lamApp{t_1}{t_2}) & = \lamApp{([x := s]t_1)}{([x := s]t_2)} &
\end{array}
\end{equation*}
Merk op dat substitutie in deze definitie een partiële functie is: als \(y \in FV(s)\) dan zou \([x := s](\lamAbs{y}{t})\) geen waarde hebben.
Dit is het geval, omdat de \(y\) in \(s\) gebonden zou woorden door de \(\lambda\)-abstractie.
Om dit op te lossen kan \(y\) en elk voorkomen van \(y\) in \(t\) door een nog niet gebonden variabele vervangen worden;
dit noemt men \(\alpha\)-conversie.
Er wordt daarom gesproken van een substitutie `tot op \(\alpha\)-conversie na';
zie voor een uitgebreidere beschrijving en uitleg \cite[pp. 69--71]{Pierce02}.
Om dit probleem op een geraffineerder wijze op te lossen, kan bijvoorbeeld gebruikt gemaakt worden van \emph{De Bruijn-indices}, een representatie van termen die geen gebruik maakt van variabelen met namen, maar van getallen die verwijzen naar de \(\lambda\)-abstractie waarin de variabele gebonden is (\cite{DeBruijn72}; \cite[hoofdstuk 6]{Pierce02}).
Hier is gekozen om geen gebruik te maken van De Bruijn-indices, omdat substituties over termen voor het vervolg niet nodig zijn.

\subsubsection{Semantiek}
Met de definitie van substituties kan de uitvoerstrategie gedefinieerd worden aan de hand van \emph{natuurlijke operationele} semantiek ofwel \emph{bigstep}-semantiek.
De regel \(t \Downarrow t'\) voor termen \(t\) en \(t'\) betekent: \(t\) evalueert tot \(t'\).
De evaluatieregels zijn als volgt:
\begin{equation*}
  \inference[\textsc{E-Abs}: ]{}{
    \lamAbs{x}{t} \Downarrow \lamAbs{x}{t}
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{E-App}: ]{
      t_1 \Downarrow \lamAbs{x}{t_1'} & t_2 \Downarrow t_2' & [x := t_2']t_1' \Downarrow t'
  }{
    \lamApp{t_1}{t_2} \Downarrow t'
  }
\end{equation*}

\subsubsection{De Church-codering}\label{sec:church}
In de \lamcalc{} kunnen waarden als (natuurlijke) getallen en booleaanse waarden zonder veel moeite geïmplementeerd worden.
Zo kunnen de booleaanse waarden als volgt geschreven worden:
\begin{align*}
  \lamTrue & = \lamAbs{t}{\lamAbs{f}{t}} \\
  \lamFalse & = \lamAbs{t}{\lamAbs{f}{f}}
\end{align*}
Een \textbf{if}-\textbf{then}-\textbf{else}-functie kan dan zo geïmplementeerd worden:
\begin{equation*}
  \lamvar{ifThenElse} = \lamAbs{i}{\lamAbs{t}{\lamAbs{e}{\lamApp{i}{t}{e}}}}
\end{equation*}
(Merk op dat \(\lamApp{\lamvar{ifThenElse}}{i}{t}{e} === \lamApp{i}{t}{e}\).)

Getallen worden gecodeerd door middel van het herhaaldelijk toepassen van functies.
De definities van \(0\) en \(\lamvar{succ}\) (voor \emph{succesor}, opvolger), de functie die \(1\) optelt bij haar argument, zijn:
\begin{align*}
  0 & = \lamAbs{f}{\lamAbs{x}{x}} \\
  \lamvar{succ} & = \lamAbs{n}{\lamAbs{f}{\lamAbs{x}{\lamApp{f}{\lamApp{n}{f}{x}}}}}
\end{align*}
Voor een natuurlijk getal \(n\) geldt dat de codering in de \lamcalc{} \(\lamAbs{f}{\lamAbs{x}{\lamApp{f^{(n)}}{x}}}\) is (\cite[p. 13]{Barendregt00});
hier is \(f^{(k)}\) voor een natuurlijk getal \(k\) de functie die \(f\) precies \(k\) keer toepast op haar argument.

Zie \cite[pp. 58--63]{Pierce02} en \cite[hoofdstuk 3]{Barendregt00} voor de definities van bewerkingen als logische conjunctie en disjunctie, optelling, vermenigvuldiging, etc. voor de Church-codering.

\subsection{De simpel getypeerde lambda-calculus}
De simpel getypeerde \lamcalc{} is een uitbreiding van de ongetypeerde \lamcalc{}, bijvoorbeeld door de toevoeging van getallen of booleaanse waarden.
In de rest van deze paragraaf zullen die waarden met respectievelijk de naam \(\lamInt\) en \(\lamBool\) toegevoegd worden.
De syntaxis en typeringsregels worden bijgewerkt om voor de simpel getypeerde \lamcalc{} juist te zijn.

\subsubsection{Syntaxis}
Naast termen heeft de simpel getypeerde \lamcalc{} ook typen:
\begin{align*}
  \tau ::=\ & \lamBool & \text{(type van booleans)} \\
         |\ & \lamInt  & \text{(type van getallen)} \\
         |\ & \tau -> \tau  & \text{(type van functies)}
\end{align*}
De definitie van termen verschilt ook, door de toevoeging van getal- en booleaanse waarden, en in de syntaxis voor \(\lambda\)-abstracties.
Bovendien wordt een \textbf{if}-\textbf{then}-\textbf{else}-term toegevoegd, die het toevoegen van booleans enigszins nuttig maakt.
Voor getallen wordt alleen de optellingsbewerking toegevoegd;
de syntaxis, typering en semantiek voor andere bewerkingen op getallen lijken sterk op die van optelling.
\begin{align*}
  t ::=\ & \lamTrue\ |\ \lamFalse & \text{(booleaanse waarden)} \\
      |\ & n & \text{als } n \in \mathbb{N} \text{ (getallen)} \\
      |\ & x & \text{(variabele)} \\
      |\ & \lamAbs{x:\tau}{t} & \text{(abstractie)} \\
      |\ & \lamApp{t}{t} & \text{(applicatie)} \\
      |\ & \lamIfThenElse{t}{t}{t} & \text{(voorwaarde)} \\
      |\ & t + t & \text{(optelling)}
\end{align*}

\subsubsection{Typering}
Typeringsregels worden beschreven aan de hand van natuurlijke deductie met afleidingsregels;
boven de streep staan de premissen, eronder de conclusie.
De typeringsrelatie \(\Gamma |- t : \tau\) kan gelezen worden als: uit de vooronderstellingen in \(\Gamma\) kan afgeleid worden dat de term \(t\) type \(\tau\) heeft (\cite[\S 9.2]{Pierce02}).
\(\Gamma\) is een verzameling van paren \(x : \tau\), waarin \(x\) een variabele is en \(\tau\) het type van die variabele.
Als er voor het symbool \(|-\) niets staat, kan daar \(\emptyset\) ingevuld worden: er zijn geen vooronderstellingen nodig om het resultaat af te leiden.
De typeringsregels voor de simpel getypeerde \lamcalc{} zijn als volgt:
\begin{equation*}
  \inference[\textsc{T-True}: ]{}{
    |- \lamTrue : \lamBool
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-False}: ]{}{
    |- \lamFalse : \lamBool
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Int}: ]{
    n \in \mathbb{N}
  }{
    |- n : \lamInt
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Var}: ]{
    x : \tau \in \Gamma
  }{
    \Gamma |- x : \tau
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Abs}: ]{
    \Gamma, x : \tau_1 |- t_2 : \tau_2
  }{
    \Gamma |- \lamAbs{x:\tau_1}{t_2} : \tau_1 -> \tau_2
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-App}: ]{
    \Gamma |- t_1 : \tau_{1, 1} -> \tau_{1, 2} & \Gamma |- t_2 : \tau_{1, 1}
  }{
    \Gamma |- \lamApp{t_1}{t_2} : \tau_{1, 2}
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-If}: ]{
    \Gamma |- t_1 : \lamBool & \Gamma |- t_2 : \tau & \Gamma |- t_3 : \tau
  }{
    \Gamma |- \lamIfThenElse{t_1}{t_2}{t_3} : \tau
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Plus}: ]{
    \Gamma |- t_1 : \lamInt & \Gamma |- t_2 : \lamInt
  }{
    \Gamma |- t_1 + t_2 : \lamInt
  }
\end{equation*}

\subsection{De polymorfe lambda-calculus}
\subsubsection{Motivatie}
De simpel getypeerde \lamcalc{} heeft een voordeel over de ongetypeerde \lamcalc{}: de juistheid van een programma kan (tot op zekere hoogte) statisch (vóór het uitvoeren) gecontroleerd worden.
Een nadeel is echter, dat deze typering zorgt voor duplicatie:
waar in de ongetypeerde \lamcalc{} nog \(\lamAbs{x}{x}\) gebruikt kan worden als identiteitsfunctie, moet in de simpel getypeerde \lamcalc{} voor elk type een andere identiteitsfunctie geschreven worden.
Dat levert definities als de volgende op:
\begin{align*}
  \lamvar{id}_{\lamInt} & = \lamAbs{x : \lamInt}{x} \\
  \lamvar{id}_{\lamBool} & = \lamAbs{x : \lamBool}{x} \\
  \lamvar{id}_{\lamInt -> \lamInt} & = \lamAbs{x : \lamInt -> \lamInt}{x} \\
  \lamvar{id}_{\lamBool -> \lamInt} & = \lamAbs{x : \lamBool -> \lamInt}{x} \\
  & \vdots
\end{align*}
Dit gaat in tegen het \emph{abstractieprincipe}:
\begin{quote}\textquote{%
  \textsc{Abstraction principle}: Each significant piece of functionality in a program should be implemented in just one place in the source code.
  Where similar functions are carried out by distinct pieces of code, it is generally beneficial to combine them into one by abstracting out the varying parts.} \\
  (\cite[p. 339]{Pierce02})
\end{quote}

De oplossing voor het hierboven beschreven probleem is de \emph{polymorfe \lamcalc{}}.

\subsubsection{Syntaxis}
De syntaxis van typen in de polymorfe \lamcalc{} bevat ten opzichte van de simpel getypeerde \lamcalc{} één toevoeging: typevariabelen.
De regels zijn zo, waar \(\alpha\) een willekeurige typevariabele voorstelt:
\begin{align*}
  \tau ::=\ & \lamBool      & \text{(type van booleans)} \\
         |\ & \lamInt       & \text{(type van getallen)} \\
         |\ & \tau -> \tau  & \text{(type van functies)} \\
         |\ & \alpha        & \text{(typevariabele)}
\end{align*}
De vrije variabelen \(FV(\tau)\) van een type \(\tau\) zijn alle typevariabelen die voorkomen in \(\tau\).

Ook de syntaxis van termen bevat een aanpassing;
de \(\lambda\)-abstracties bevatten geen expliciete typen meer, zodat de termen volledig impliciet getypeerd zijn:
\begin{align*}
  t ::=\ & \lamTrue\ |\ \lamFalse & \text{(booleaanse waarden)} \\
      |\ & n & \text{als } n \in \mathbb{N} \text{ (getallen)} \\
      |\ & x & \text{(variabele)} \\
      |\ & \lamAbs{x}{t} & \text{(abstractie)} \\
      |\ & \lamApp{t}{t} & \text{(applicatie)} \\
      |\ & \lamIfThenElse{t}{t}{t} & \text{(voorwaarde)} \\
      |\ & t + t & \text{(optelling)}
\end{align*}

Er bestaan in de polymorfe \lamcalc{}, naast typen, ook \emph{typeschema's}.
De syntaxis van typeschema's is als volgt:
\begin{equation*}
  \sigma ::= \forall \vec{\alpha}.\ \tau
\end{equation*}
Hierin is \(\vec{\alpha}\) een (mogelijk lege) verzameling van typevariabelen.
De universele kwantor duidt aan dat voor de typevariabelen die volgen elk type ingevuld kan worden.
De vrije variabelen van een typeschema zijn:
\begin{equation*}
  FV(\forall \vec{\alpha}. \tau) = FV(\tau) \setminus \vec{\alpha}
\end{equation*}

De regel voor typeschema's zou ook in de regels voor typen opgenomen kunnen worden;
die taal wordt \emph{System F} genoemd, waarin een type als \(\forall \alpha.\ (\forall \beta.\ \beta -> \beta) -> \alpha -> \alpha\) syntactisch correct is (en er bestaat ook een term met dat type).
Omdat volledig type-afleiding (Engels: \emph{type inference}) voor System F onbeslisbaar is, en omdat de taal SSML wel gebruik maakt van type-afleiding, is er hier gekozen om typen en typeschema's gescheiden te houden.
Het typesysteem dat de typering van deze taal beschrijft, is het \emph{Hindley-Damas-Milner-typesysteem} (\cite{Damas82}).

Deze \lamcalc{} lost het hierboven beschreven probleem op: er bestaat een universele identiteitsfunctie, geschreven als
\[
  \lamvar{id} = \lamAbs{x}{x} \text{,}
\]
met het typeschema
\[
  \forall \alpha.\ \alpha -> \alpha \text{,}
\]
die op alle waarden toegepast kan worden.

\subsubsection{Typering}
De typering van de polymorfe \lamcalc{} is ingewikkelder dan die van de simpel getypeerde \lamcalc{}.
Zo moet er gecontroleerd worden of alle termen die als een bepaald type \(\alpha\) voorkomen, hetzelfde (concrete) type hebben.
Hiervoor is oorspronkelijk algoritme \(\mathcal{W}\) (\cite{Milner78}) ontworpen, maar in het vervolg wordt de typering beschreven aan de hand van een ander algoritme: er worden voorwaarden verzameld en daarna opgelost.

Een voorwaarde (Engels: \emph{constraint}) heeft de vorm \(\tau_1 === \tau_2\), voor types \(\tau_1\) en \(\tau_2\).
Deze stelt dat \(\tau_1\) en \(\tau_2\) hetzelfde type moeten zijn.
Een oplossing voor een verzameling voorwaarden \(C\) is een verzameling van typevariabelen en typen, zodat aan alle voorwaarden uit \(C\) is voldaan.
Voor de voorwaarden \(C = \{ \alpha -> \beta === \lamInt -> \lamBool \}\) is bijvoorbeeld \(\{ \alpha = \lamInt, \beta = \lamBool \}\) een oplossing.
Er bestaan ook voorwaarden waarvoor geen oplossing bestaat, zoals \(\{ \alpha === \alpha -> \alpha \}\).

Wanneer alle voorwaarden bepaald zijn, wordt er een substitutie over typen gegenereerd.
Het bepalen van de substitutie aan de hand van voorwaarden wordt \emph{verenigen} (Engels: \emph{unify}) genoemd.
Een substitutie \(\mathcal{S}\) \emph{verenigt} een voorwaarde \(\tau_1 === \tau_2\) als \(\mathcal{S} \tau_1 = \mathcal{S} \tau_2\).
Een substitutie \(\mathcal{S}\) verenigt een verzameling voorwaarden \(C\) als \(\mathcal{S}\) elke voorwaarde uit \(C\) verenigt.
Het algoritme dat een verenigende substitutie bepaalt voor een verzameling voorwaarden werkt als volgt (\cite[\S 22.4]{Pierce02}; \cite[\S 6.5.2]{Dijkstra01}):
\begin{align*}
  \mathit{verenig}(\emptyset) =\ & [\ ] \\
  \mathit{verenig}(\{\tau_1 === \tau_2\} \cup C') =\ & \mathit{verenig}(C'), \\
      & \quad \text{als } \tau_1 = \tau_2 \\
      %
      & \mathit{verenig}([\alpha := \tau_2] C') \circ [\alpha := \tau_2], \\
      & \quad \text{als } \tau_1 = \alpha \text{ en } \alpha \not\in FV(\tau_2) \\
      %
      & \mathit{verenig}([\alpha := \tau_1] C') \circ [\alpha := \tau_1], \\
      & \quad \text{als } \tau_2 = \alpha \text{ en } \alpha \not\in FV(\tau_1) \\
      %
      & \mathit{verenig}(C' \cup \{ \tau_{1,1} === \tau_{2,1}, \tau_{1,2} === \tau_{2, 1} \}), \\
      & \quad \text{als } \tau_1 = \tau_{1,1} -> \tau_{1,2} \text{ en } \tau_2 = \tau_{2,1} -> \tau_{2,2} \\
      & \bot, \\
      & \quad \text{anders}
\end{align*}
Hierin is \(\mathcal{S}_2 \circ \mathcal{S}_1\) een compositie van substituties \(\mathcal{S}_1\) en \(\mathcal{S}_2\).
Merk op dat \(\circ\) in het algemeen niet commutatief is, bijvoorbeeld omdat \([\beta := \alpha -> \alpha] \circ [\alpha := \gamma] = [\alpha := \gamma,\ \beta := \alpha -> \alpha]\) en \([\alpha := \gamma] \circ [\beta := \alpha -> \alpha] = [\alpha := \gamma,\ \beta := \gamma -> \gamma]\).
Als de voorwaarden niet verenigd kunnen worden, is het resultaat \(\bot\);
er geldt \(S \circ \bot = \bot \circ S = \bot\).
Zie \cite[p. 3]{Heeren02}, \cite[\S 22]{Pierce02} en \cite[\S 6.5]{Dijkstra01} voor meer over substituties over typen en het verenigen van voorwaarden.

De typeringsregels voor de polymorfe \lamcalc{} zijn als volgt.
De typeringsrelatie die hier gebruikt wordt is \(\Gamma |- t : \tau \mid_X C\).
\(\Gamma\) is hier een verzameling van paren \(x : \sigma\), waar \(x\) een variabele is en \(\sigma\) een typeschema.
Bij deze typeringsrelatie zijn, ten opzichte van de typering van de simpel getypeerde \lamcalc{}, de subscripten met een \(X\) en de \(C\) toegevoegd.
De eerste geven de \emph{verse} typevariabelen aan; dat zijn typevariabelen die gemaakt worden tijdens het typeren, vaak zo dat ze niet voorkomen in de andere typen.
De tweede geven de voorwaarden aan waaraan voldaan moet worden als \(t : \tau\).
Zie \cite[\S 22.3]{Pierce02} voor een uitgebreider en formeler beschrijving van deze typeringsrelatie.
Merk op dat niet elke regel een concreet type oplevert;
het meest algemene type wordt bepaald wanneer de gegenereerde substitutie wordt toegepast op het resultaattype (zie regel \textsc{T-Schema}):

\begin{equation*}
  \inference[\textsc{T-True}: ]{}{
    |- \lamTrue : \lamBool \mid_\emptyset \emptyset
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-False}: ]{}{
    |- \lamFalse : \lamBool \mid_\emptyset \emptyset
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Int}: ]{
    n \in \mathbb{N}
  }{
    |- n : \lamInt \mid_\emptyset \emptyset
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Var}: ]{
    x : \sigma \in \Gamma \\
    \tau = \mathit{instantieer}_X(\sigma)
  }{
    \Gamma |- x : \tau \mid_X \emptyset
  }
\end{equation*}
\(\mathit{instantieer}_X(\forall \vec{\alpha}.\ \tau)\) genereert voor elke \(\alpha_i\) in \(\tau\) een nieuwe typevariabele \(\beta_i\), en geeft dan \(\tau\) terug waarin elke \(\alpha_i\) voor \(\beta_i\) is gesubstitueerd.
Op \(\tau\) wordt dus de substitutie \([\alpha_1 := \beta_1, \ldots, \alpha_n := \beta_n]\) toegepast.
Deze functie is nodig, omdat \(\Gamma\) uit paren van variabelen en \emph{typeschema's} bestaat, terwijl de typeringsregels \emph{typen} als resultaat hebben.
Een variabele met type \(\forall \alpha.\ \alpha -> \alpha\) kan ook voorkomen met type \(\forall \beta.\ \beta -> \beta\).
\begin{equation*}
  \inference[\textsc{T-Abs}: ]{
    \Gamma, x : \alpha |- t_2 : \tau_2 \mid_{X \cup \{\alpha\}} C \\
    \alpha \not\in X, \tau_2
  }{
    \Gamma |- \lamAbs{x}{t_2} : \alpha -> \tau_2 \mid_{X \cup \{\alpha\}} C
  }
\end{equation*}
In het paar \(x : \alpha\) dat aan \(\Gamma\) wordt toegevoegd, kan \(\alpha\) preciezer geschreven worden als \(\forall \emptyset.\ \alpha\);
het is dus een typeschema.
\begin{equation*}
  \inference[\textsc{T-App}: ]{
    \Gamma |- t_1 : \tau_1 \mid_{X_1} C_1 & \Gamma |- t_2 : \tau_2 \mid_{X_2} C_2 \\
    X_1 \cap X_2 = X_1 \cap FV(\tau_2) = X_2 \cap FV(\tau_1) = \emptyset \\
    \alpha \not\in X_1, X_2, \tau_1, \tau_2, C_1, C_2, \Gamma \\
    C' = C_1 \cup C_2 \cup \{ \tau_1 === \tau_2 -> \alpha \}
  }{
    \Gamma |- \lamApp{t_1}{t_2} : \alpha \mid_{X_1 \cup X_2 \cup \{\alpha\}} C'
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-If}: ]{
    \Gamma |- t_1 : \tau_1 \mid_{X_1} C_1 \\
    \Gamma |- t_2 : \tau_2 \mid_{X_2} C_2 \\
    \Gamma |- t_3 : \tau_3 \mid_{X_3} C_3 \\
    X_1 \cap X_2 = X_2 \cap X_3 = X_3 \cap X_1 = \emptyset \\
    C' = C_1 \cup C_2 \cup C_3 \cup \{ \tau_1 === \lamBool, \tau_2 === \tau_3 \}
  }{
    \Gamma |- \lamIfThenElse{t_1}{t_2}{t_3} : \tau_2 \mid_{X_1 \cup X_2 \cup X_3} C'
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Plus}: ]{
    \Gamma |- t_1 : \tau_1 \mid_{X_1} C_1 & \Gamma |- t_2 : \tau_2 \mid_{X_2} C_2 \\
    X_1 \cap X_2 = \emptyset \\
    C' = C_1 \cup C_2 \cup \{ \tau_1 === \lamInt, \tau_2 === \lamInt \}
  }{
    \Gamma |- t_1 + t_2 : \lamInt \mid_{X_1 \cup X_2} C'
  }
\end{equation*}

Het typeschema wordt dan zo bepaald:
\begin{equation*}
  \inference[\textsc{T-Schema}: ]{
    \Gamma |- t : \tau \mid_X C \\
    \mathcal{S} = \mathit{verenig}(C) \\
    \sigma = \mathit{generaliseer}(\Gamma, \mathcal{S} \tau)
  }{
    \Gamma |- t : \sigma
  }
\end{equation*}
Hierin is \(\mathit{generaliseer}(\Gamma, \tau)\) een functie die het typeschema van \(\tau\) als resultaat geeft, voor alle typevariabelen die niet in \(\Gamma\) voorkomen:
\begin{align*}
  \mathit{generaliseer}(\Gamma, \tau) & = \forall \vec{\alpha}.\ \tau \\
  & \quad \text{waar } \vec{\alpha} = FV(\tau) \setminus FV(\Gamma)
\end{align*}
De verzameling van vrije variabelen van een omgeving \(\Gamma\) is de vereniging van de vrije variabelen van alle typeschema's in \(\Gamma\):
\begin{equation*}
  FV(\Gamma) = \bigcup_{x : \sigma \in \Gamma} FV(\sigma)
\end{equation*}

\subsubsection{Let-polymorfisme}
Aan de polymorfe \lamcalc{} wordt vaak ook \emph{let-polymorfisme} toegevoegd (\cite[\S 22.7]{Pierce02}).
Dit maakt het mogelijk om een term twee keer te gebruiken als twee verschillende typen.
In de polymorfe \lamcalc{} zou de term
\[
  \lamApp{
    (\lamAbs{\lamvar{id}}{
      \lamApp{
        f
      }{
        (\lamApp{\lamvar{id}}{1})
      }{
        (\lamApp{\lamvar{id}}{\lamTrue})
      }
    })
  }{
    (\lamAbs{x}{x})
  }
\]
voor een of andere functie \(f\) niet correct getypeerd zijn.
De typen van \(1\) en \(\lamTrue\) zouden namelijk verenigd worden, omdat ze beiden als parameter aan \(\lamvar{id}\) gegeven worden, wat een typeringsfout oplevert.

Let-polymorfisme lost dit probleem op door een \textbf{let}-term toe te voegen:
\begin{align*}
  t ::=\ & \ldots & \\
      |\ & \lamLet{x}{t}{t} & \text{(\textbf{let}-term)} \\
\end{align*}
De term hierboven kan dan geschreven worden als
\[
  \lamLet{\lamvar{id}}{\lamAbs{x}{x}}{\lamApp{f}{(\lamApp{\lamvar{id}}{1})}{(\lamApp{\lamvar{id}{\lamTrue}})}}
\]

De typering voor \textbf{let}-termen is zo:
\begin{equation*}
  \inference[\textsc{T-Let}: ]{
    \Gamma |- [x -> t_1]t_2 : \tau_2 \mid_X C
  }{
    \Gamma |- \lamLet{x}{t_1}{t_2} : \tau_2 \mid_X C
  }
\end{equation*}

In de praktijk wordt niet gebruik gemaakt van deze typeringsregel \textsc{T-Let}, omdat het uitvoeren ervan veel tijd en geheugen kost door de substitutie.
In plaats daarvan wordt \textsc{T-Let} als volgt gedefinieerd.
Deze regels zijn formeel equivalent (\cite[p. 333]{Pierce02}):
\begin{equation*}
  \inference[\textsc{T-Let}: ]{
    \Gamma |- t_1 : \tau_1 \mid_{X_1} C_1 \\
    \mathcal{S} = \mathit{verenig}(C_1) \\
    \tau_1' = \mathcal{S} \tau_1 \\
    \sigma = \mathit{generaliseer}(\mathcal{S} \Gamma, \tau_1') \\
    \Gamma, x : \sigma |- t_2 : \tau_2 \mid_{X_2} C_2 \\
    X_1 \cap X_2 = \emptyset
  }{
    \Gamma |- \lamLet{x}{t_1}{t_2} : \tau_2 \mid_{X_1 \cup X_2} C_2
  }
\end{equation*}
Eerst wordt het typeschema \(\sigma\) van \(t_1\) bepaald, zoals in \textsc{T-Schema} gebeurt.
Het type van \(t_2\) wordt dan bepaald door \(t_1 : \sigma\) aan \(\Gamma\) toe te voegen.

\subsection{SKI-combinatorcalculus}\label{sec:theoretische-inleiding:ski}
De SKI-combinatorcalculus is een systeem van combinatorische logica dat berust op slechts vier bouwstenen (terug te brengen tot twee, waarover later meer).
Deze bouwstenen zijn de \(\skiS\)-, \(\skiK\)-, \(\skiI\)- en \(\skiY\)-combinatoren, gegeven in de volgende vergelijkingen (\cite{Turner79}; \cite[p. 12]{Barendregt00}):
\begin{align}
  \lamApp{\skiS}{f}{g}{x} & = \lamApp{f}{x}{(\lamApp{g}{x})} \tag{S}\label{eqn:S} \\
  \lamApp{\skiK}{x}{y} & = x \tag{K}\label{eqn:K} \\
  \lamApp{\skiI}{x} & = x \tag{I}\label{eqn:I} \\
  \lamApp{\skiY}{f} & = \lamApp{f}{(\lamApp{\skiY}{f})} \tag{Y}\label{eqn:Y}
\end{align}

Daarnaast worden vaak ook booleaanse waarden gedefinieerd als:
\begin{align*}
  \skiT & === \skiK \\
  \skiF & === \skiS \skiK
\end{align*}
Logische conjunctie en disjunctie worden dan gedefinieerd als:
\begin{align*}
  \wedge & === \skiF \\
  \vee & === \skiT
\end{align*}

De combinatoren nemen een aantal argumenten en geven daarna iets terug, wat de \emph{reductie} van de combinator genoemd wordt.

\subsubsection{Abstractie}
Elke term van de \lamcalc{} zonder vrije variabelen kan in SKI-combinatoren gecodeerd worden (\cite{Turner79}).
Deze vertaling wordt \emph{abstractie} genoemd.

Abstractie wordt als volgt met een vertaalschema (\cite[\S 5.2]{Dijkstra01}) gedefinieerd (\cite{Turner79}; \cite[\S 16.1]{Jones86}).
\(\scheme[\ x\ ]{A}{t}\) is de functie die variabele \(x\) uit term \(t\) abstraheert:
\begin{align*}
  \scheme[\ x\ ]{A}{\lamApp{t_1}{t_2}} & === \lamApp{\skiS}{(\scheme[\ x\ ]{A}{t_1})}{(\scheme[\ x\ ]{A}{t_2})} \\
  \scheme[\ x\ ]{A}{x} & === \skiI \\
  \scheme[\ x\ ]{A}{y} & === \lamApp{\skiK}{y}
\end{align*}
In de laatste regel staat \(y\) voor elke term niet gelijk aan \(x\).

De vertaling van termen van de ongetypeerde \lamcalc{} naar SKI-combinatoren is dan als volgt.
Hierin is \(\scheme{C}{t}\) de functie die term \(t\) vertaalt naar een SKI-term:
\begin{align*}
  \scheme{C}{\lamApp{t_1}{t_2}} & === \lamApp{\scheme{C}{t_1}}{\scheme{C}{t_2}} \\
  \scheme{C}{\lamAbs{x}{t_1}} & === \scheme[\ x\ ]{A}{\ \scheme{C}{t_1}\ } \\
  \scheme{C}{x} & === x
\end{align*}

\subsubsection{Reductie}
Hieronder is de reductie van de termen \(\skiS \skiK \skiK x\) en \(\skiI x\) gegeven, waarin \(x\) een willekeurige term voorstelt.
Aangezien de producten van beide reducties gelijk zijn aan \(x\), kan geconcludeerd worden dat \(\skiS \skiK \skiK === \skiI\).
De combinator \(\skiI\) is dus overbodig, maar omdat de notatie compacter en duidelijker is, zal in het vervolg gebruik worden gemaakt van \(\skiI\).
\begin{align*}
     &\ \skiS \skiK \skiK x & \\
  => &\ \skiK x (\skiK x) & \text{(per definitie \ref{eqn:S})} \\
  => &\ x & \text{(per definitie \ref{eqn:K})} \\
     & & \\
     &\ \skiI x & \\
  => &\ x & \text{(per definitie \ref{eqn:I})}
\end{align*}

Ook \(\skiY\) is geen elementaire combinator, namelijk:
\begin{equation*}
  \skiY === \skiS (\skiK (\skiS \skiI \skiI)) (\skiS (\skiS (\skiK \skiS) \skiK) (\skiK (\skiS \skiI \skiI)))
\end{equation*}
Dat is het geval omdat \(\skiY\) in de \lamcalc{} geschreven kan worden als:
\begin{equation*}
  \skiY === (\lamAbs{x}{\lamAbs{y}{xyx}})(\lamAbs{y}{\lamAbs{x}{y(xyx)}})
\end{equation*}
Daarom wordt \(\skiY\) niet gebruikt in de definities van \(\mathcal{A}\) en \(\mathcal{C}\).