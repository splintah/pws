\section{SSML}\label{sec:ssml}
SSML (\textbf{S}ybrand en \textbf{S}plinters \textbf{ML}) is een pure functionele programmeertaal, beïnvloed door onder meer ML (\cite{Milner78}), Haskell en OCaml.
Net zoals deze talen is SSML gebaseerd op de polymorfe \lamcalc{}, met de toevoeging van \emph{let}-polymorfisme.

Daarnaast bevat SSML \textbf{fix}- (van \emph{dekpunt}, Engels: \emph{fix point}) en \textbf{let rec}-termen, waarmee recursieve functies gedefinieerd kunnen worden.
Origineel was alleen \textbf{fix} beschikbaar, maar later is ook \textbf{let rec} toegevoegd, omdat \textbf{let rec}-termen vaak makkelijker te schrijven en lezen zijn.

\textbf{fix} kan gezien worden als een functie met type \(\forall \alpha.\ (\alpha -> \alpha) -> \alpha\).
\textbf{fix} kan echter niet geïmplementeerd worden in de polymorfe \lamcalc{}, dus moet deze handmatig in de compiler worden geïmplementeerd.
De definitie van \textbf{fix} kan wel geschreven worden met behulp van een \textbf{let rec}-term, namelijk zo:
\[
  \textbf{fix} = \ssmlAbs{f}{\lamLetRec{x}{\lamApp{f}{x}}{x}}
\]
Dat toont aan dat \textbf{let rec} minstens zo `krachtig' is als \textbf{fix}.
Ze zijn even krachtig, omdat elke \textbf{let rec}-term vertaald kan worden naar een term die \textbf{fix} gebruikt;
zie daarvoor \S\ref{sec:ssml-naar-ski:vertaling}.

De (strenge, statische) typering van SSML lijkt ook op die van de polymorfe \lamcalc{} met \emph{let}-polymorfisme, maar is uitgebreid met regels voor recursieve termen.

\subsection{Syntaxis}
Voor het ontleden van SSML-programma's wordt gebruik gemaakt van \emph{parsercombinatoren}, gebaseerd op \cite{Jeuring17}.
De syntaxis van SSML-programma's (\(p\)), -termen (\(t\)) en -operatoren (\(op\)) is zo:
\begin{align*}
  p ::=\ & \epsilon & \text{(leeg programma)} \\
      |\ & x = t; p & \text{(declaratie)}
\end{align*}
\begin{align*}
  t ::=\ & \lamTrue\ |\ \lamFalse & \text{(booleaanse waarden)} \\
      |\ & n & \text{als } n \in \mathbb{N} \text{ (getallen)} \\
      |\ & x & \text{(variabele)} \\
      |\ & \ssmlAbs{x}{t} & \text{(abstractie)} \\
      |\ & \lamApp{t}{t} & \text{(applicatie)} \\
      |\ & \lamIfThenElse{t}{t}{t} & \text{(voorwaarde)} \\
      |\ & \lamLet{x}{t}{t} & \text{(\textbf{let}-term)} \\
      |\ & \lamLetRec{x}{t}{t} & \text{(\textbf{let rec}-term)} \\
      |\ & \lamFix{t} & \text{(\textbf{fix}-term)} \\
      |\ & t\ \mathit{op}\ t & \text{(binaire operatie)}
\end{align*}
\begin{align*}
  \mathit{op} ::=\ & + & \text{(optelling)} \\
                |\ & - & \text{(aftrekking)} \\
                |\ & * & \text{(vermenigvuldiging)} \\
                |\ & / & \text{(deling)} \\
                |\ & \% & \text{(modulus)} \\
                |\ & \&\& & \text{(logische conjunctie)} \\
                |\ & || & \text{(logische disjunctie)} \\
                |\ & < & \text{(kleiner dan)} \\
                |\ & <= & \text{(kleiner dan of gelijk aan)} \\
                |\ & > & \text{(groter dan)} \\
                |\ & >= & \text{(groter dan of gelijk aan)} \\
\end{align*}
(Merk op dat gelijkheid niet is opgenomen in de syntaxis.
Daarvoor zou het typesysteem van SSML uitgebreid moeten worden;
zie hoofdstuk \ref{sec:discussie} voor meer hierover.)

\subsection{Typering}\label{sec:ssml:typering}
De typering van SSML is grotendeels hetzelfde als die van de polymorfe \lamcalc{}.
Daarbij worden de volgende regels toegevoegd:
\begin{equation*}
  \inference[\textsc{T-Fix}: ]{
    \Gamma |- t : \tau \mid_X C \\
    \alpha \not\in X, \tau, C, \Gamma \\
    C' = C \cup \{ \alpha -> \alpha === \tau \}
  }{
    \Gamma |- \lamFix{t} : \alpha \mid_X C'
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-LetRec}: ]{
    \Gamma |- \lamLet{x}{\lamFix{(\ssmlAbs{x}{t_1})}}{t_2} : \tau_2 \mid_X C
  }{
    \Gamma |- \lamLetRec{x}{t_1}{t_2} : \tau_2 \mid_X C
  }
\end{equation*}
De volgende regel is alleen expliciet voor \(+\) geschreven, maar geldt ook voor elke operatie uit \(\{ -, *, /, \% \}\).
\begin{equation*}
  \inference[\textsc{T-Plus}: ]{
    \Gamma |- t_1 : \tau_1 \mid_{X_1} C_1 & \Gamma |- t_2 : \tau_2 \mid_{X_2} C_2 \\
    X_1 \cap X_2 = \emptyset \\
    C' = C_1 \cup C_2 \cup \{ \tau_1 === \lamInt, \tau_2 === \lamInt \}
  }{
    \Gamma |- t_1 + t_2 : \lamInt \mid_{X_1 \cup X_2} C'
  }
\end{equation*}
Zoals bij \textsc{T-Plus} is de volgende regel alleen expliciet voor \(\&\&\) geschreven, maar geldt ook voor \(||\).
\begin{equation*}
  \inference[\textsc{T-Conj}: ]{
    \Gamma |- t_1 : \tau_1 \mid_{X_1} C_1 & \Gamma |- t_2 : \tau_2 \mid_{X_2} C_2 \\
    X_1 \cap X_2 = \emptyset \\
    C' = C_1 \cup C_2 \cup \{ \tau_1 === \lamBool, \tau_2 === \lamBool \}
  }{
    \Gamma |- t_1\ \&\&\ t_2 : \lamBool \mid_{X_1 \cup X_2} C'
  }
\end{equation*}
En ook de volgende regel geldt ook voor elke operatie uit \(\{ <=, >, >= \}\).
\begin{equation*}
  \inference[\textsc{T-Lt}: ]{
    \Gamma |- t_1 : \tau_1 \mid_{X_1} C_1 & \Gamma |- t_2 : \tau_2 \mid_{X_2} C_2 \\
    X_1 \cap X_2 = \emptyset \\
    C' = C_1 \cup C_2 \cup \{ \tau_1 === \lamInt, \tau_2 === \lamInt \}
  }{
    \Gamma |- t_1 < t_2 : \lamBool \mid_{X_1 \cup X_2} C'
  }
\end{equation*}

Naast de typeringsregels controleert de typechecker ook of er een functie met de naam \(\lamvar{main}\) bestaat.
Dit is belangrijk voor de uitvoering van programma's, zoals in \S\ref{sec:ssml-naar-ski:vertaling} is beschreven.