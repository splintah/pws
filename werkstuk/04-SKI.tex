\section{SKI}\label{sec:ski}
In \S\ref{sec:theoretische-inleiding:ski} zijn SKI-combinatoren geïntroduceerd;
in deze paragraaf zal de implementatie van SKI-combinatoren voor de SSML-compiler besproken worden.

De SKI-combinatorcalculus is verrijkt met getallen en ingebouwde functies.
Getallen zouden als functies gerepresenteerd kunnen worden, zoals aangetoond in paragraaf \ref{sec:church}, maar ten behoeve van snelheid bij het uitvoeren worden getallen ingebouwd, met de gebruikelijke representatie (een lijst van bits).
De ingebouwde functies werken op de getallen.

Booleaanse waarden, in tegenstelling tot getalwaarden, worden wel als functies gecodeerd, volgens:
\begin{align*}
  \skiT x y = x & <=> \skiT === \skiK \\
  \skiF x y = y & <=> \skiF === \skiS \skiK
\end{align*}

\subsection{Representatie}
SKI-expressies worden in de interpretator gerepresenteerd als een graaf.

De expressies worden volgens deze syntaxis gecodeerd:
\begin{align*}
  e ::=\ & e\ e & \text{(applicatie)} \\
      |\ & \skiS & \text{(S-combinator)} \\
      |\ & \skiK & \text{(K-combinator)} \\
      |\ & \skiI & \text{(I-combinator)} \\
      |\ & \skiY & \text{(Y-combinator)} \\
      |\ & n & \text{(getal)} \\
      |\ & f & \text{(functie)}
\end{align*}
\begin{align*}
  n ::=\ & k & \text{als } k \in \mathbb{Z}, -2^{63} \leqslant k \leqslant 2^{63} - 1 \text{ (64-bits getal met teken)}
\end{align*}
\begin{align*}
  f ::=\ & \skifn{add} & \text{(optellingsfunctie)} \\
      |\ & \skifn{sub} & \text{(aftrekkingsfunctie)} \\
      |\ & \skifn{mul} & \text{(vermenigvuldigingsfunctie)} \\
      |\ & \skifn{div} & \text{(delingsfunctie)} \\
      |\ & \skifn{mod} & \text{(modulo-functie)} \\
      |\ & \skifn{eq} & \text{(\(=\)-functie)} \\
      |\ & \skifn{lt} & \text{(\(<\)-functie)} \\
      |\ & \skifn{le} & \text{(\(<=\)-functie)} \\
      |\ & \skifn{gt} & \text{(\(>\)-functie)} \\
      |\ & \skifn{ge} & \text{(\(>=\)-functie)}
\end{align*}

\subsection{Uitvoering}
De efficiëntie van dit systeem berust er onder andere op dat er geen onnodig werk plaats vindt;
zo wordt de \(y\) in \(\skiK x y = x\) niet uitgevoerd omdat de waarde ervan niet van belang is.
Ook wordt er geen dubbel werk verzet: als door \(\skiS\) termen gekopieerd worden, wordt slechts een \emph{pointer} naar het origineel opgeslagen; zo ontstaat een graaf van het programma.
Het uitvoeren betekent uiteindelijk deze graaf reduceren.

\subsubsection {Representatie als graaf}
Om de graaf te representeren gebruikt het programma een simpele datastructuur:
\begin{align*}
    \mathit{Element} =\ & * \mathit{Combinator} & \text{(pointer naar combinator)}
\end{align*}
\begin{align*}
    \mathit{Combinator} =\ & \skiS \\
                   |\ & \skiK \\
                   |\ & \skiI \\
                   |\ & \skiY \\
                   |\ & (\mathit{Element}, \mathit{Element}) & \text{(applicatie)} \\
                   |\ & n & \text{(getal)} \\
                   |\ & f & \text{(functie)}
\end{align*}

Hierbij wordt \(\skiS \skifn{add} (\skiK 4) 6 = 10\) als volgt gerepresenteerd:
\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
    app/.style={rectangle split, rectangle split horizontal, rectangle split parts=2, thick, draw=black},
  ]
    \node[app] (app1) {};
    \draw[fill] ($(app1.east)!0.5!(app1.text split)$) circle (0.03);
    \draw[fill] ($(app1.west)!0.5!(app1.text split)$) circle (0.03);
      \node[app] (app2) [below left=of app1] {};
      \draw[fill] ($(app2.east)!0.5!(app2.text split)$) circle (0.03);
      \draw[fill] ($(app2.west)!0.5!(app2.text split)$) circle (0.03);
      \draw[->] ($(app1.west)!0.5!(app1.text split)$) -- (app2.north);
        \node[app] (app3) [below left=of app2] {};
        \draw[fill] ($(app3.east)!0.5!(app3.text split)$) circle (0.03);
        \draw[fill] ($(app3.west)!0.5!(app3.text split)$) circle (0.03);
        \draw[->] ($(app2.west)!0.5!(app2.text split)$) -- (app3.north);
          \node[knoop] (S) [below left=of app3] {\(\skiS\)};
          \draw[->] ($(app3.west)!0.5!(app3.text split)$) -- (S.north east);
          \node[knoop] (add) [right=of S] {\(\skifn{add}\)};
          \draw[->] ($(app3.east)!0.5!(app3.text split)$) -- (add.north);
        \node[app] (app4) [below right=of app2] {};
        \draw[fill] ($(app4.east)!0.5!(app4.text split)$) circle (0.03);
        \draw[fill] ($(app4.west)!0.5!(app4.text split)$) circle (0.03);
        \draw[->] ($(app2.east)!0.5!(app2.text split)$) -- (app4.north);
          \node[knoop] (K) [below left=of app4] {\(\skiK\)};
          \draw[->] ($(app4.west)!0.5!(app4.text split)$) -- (K.north east);
          \node[knoop] (vier) [below right=of app4] {\(4\)};
          \draw[->] ($(app4.east)!0.5!(app4.text split)$) -- (vier.north west);
      \node[knoop] (zes) [below right=of app1] {\(6\)};
      \draw[->] ($(app1.east)!0.5!(app1.text split)$) -- (zes.north west);
  \end{tikzpicture}
  \caption{Voorbeeld van een SKI-graaf}
  \label{fig:ski:sampletree}
\end{figure}

\subsubsection{Effici\"entie van de graaf}
Interessant is vooral hoe berekeningen door de representatie als graaf nooit dubbel uitgevoerd worden. Neem de volgende graaf:
\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
    app/.style={rectangle split, rectangle split horizontal, rectangle split parts=2, thick, draw=black},
  ]
    \node[app] (app1) {};
    \draw[fill] ($(app1.east)!0.5!(app1.text split)$) circle (0.03);
    \draw[fill] ($(app1.west)!0.5!(app1.text split)$) circle (0.03);
      \node[app] (app2) [below left=of app1] {};
      \draw[fill] ($(app2.east)!0.5!(app2.text split)$) circle (0.03);
      \draw[fill] ($(app2.west)!0.5!(app2.text split)$) circle (0.03);
      \draw[->] ($(app1.west)!0.5!(app1.text split)$) -- (app2.north);
        \node[app] (app3) [below left=of app2] {};
        \draw[fill] ($(app3.east)!0.5!(app3.text split)$) circle (0.03);
        \draw[fill] ($(app3.west)!0.5!(app3.text split)$) circle (0.03);
        \draw[->] ($(app2.west)!0.5!(app2.text split)$) -- (app3.north);
          \node[knoop] (S) [below left=of app3] {\(\skiS\)};
          \draw[->] ($(app3.west)!0.5!(app3.text split)$) -- (S.north east);
          \node[knoop] (x) [below right=of app3] {\(x\)};
          \draw[->] ($(app3.east)!0.5!(app3.text split)$) -- (x.north west);
        \node[knoop] (y) [below right=of app2] {\(y\)};
        \draw[->] ($(app2.east)!0.5!(app2.text split)$) -- (y.north west);
      \node[knoop] (z) [below right=of app1] {\(z\)};
      \draw[->] ($(app1.east)!0.5!(app1.text split)$) -- (z.north west);
  \end{tikzpicture}
  \caption{Graaf van \(\skiS xyz === xz(yz)\)}
  \label{fig:ski:skiSbegin}
\end{figure}

Als dit niet als graaf uitgevoerd zou worden, dan zou \(z\) gekopieerd en tweemaal gereduceerd moeten worden: één keer voor \(xz\) en nog een keer \(yz\).
Door dit wel als een graaf uit te voeren, wordt het werk gedeeld;
zie ter verduidelijking figuur \ref{fig:ski:skiSend}.
Nadat \(z\) voor \(xz\) gereduceerd is, wordt er bij het reduceren van \(yz\) een reeds gereduceerde structuur bij \(z\) aangetroffen.
Zo wordt voorkomen dat \(z\) nogmaals gereduceerd moet worden.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
    app/.style={rectangle split, rectangle split horizontal, rectangle split parts=2, thick, draw=black},
  ]
    \node[app] (app1) {};
    \draw[fill] ($(app1.east)!0.5!(app1.text split)$) circle (0.03);
    \draw[fill] ($(app1.west)!0.5!(app1.text split)$) circle (0.03);
    \node[app] (app2) [below left=of app1] {};
    \draw[fill] ($(app2.east)!0.5!(app2.text split)$) circle (0.03);
    \draw[fill] ($(app2.west)!0.5!(app2.text split)$) circle (0.03);
    \node[app] (app3) [below right=of app1] {};
    \draw[fill] ($(app3.east)!0.5!(app3.text split)$) circle (0.03);
    \draw[fill] ($(app3.west)!0.5!(app3.text split)$) circle (0.03);
    \draw[->] ($(app1.west)!0.5!(app1.text split)$) -- (app2.north);
    \draw[->] ($(app1.east)!0.5!(app1.text split)$) -- (app3.north);

    \node[knoop] (x) [below left=of app2] {\(x\)};
    \draw[->] ($(app2.west)!0.5!(app2.text split)$) -- (x.north east);

    \node[knoop] (y) [below left=of app3] {\(y\)};
    \draw[->] ($(app3.west)!0.5!(app3.text split)$) -- (y.north east);

    \node[knoop] (z) [below=of y] {\(z\)};
    \draw[->] ($(app3.east)!0.5!(app3.text split)$) -- (z.north east);
    \draw[->] ($(app2.east)!0.5!(app2.text split)$) -- (z.north west);
  \end{tikzpicture}
  \caption{Gereduceerde graaf van \(\skiS xyz === xz(yz)\)}
  \label{fig:ski:skiSend}
\end{figure}

\subsubsection{Regels voor reductie}
Er zijn verschillende volgordes mogelijk waarin een graaf gereduceerd kan worden.
In figuur \ref{fig:ski:skiSend} kan bijvoorbeeld \(z\) als eerste gereduceerd worden, waarmee voorkomen wordt dat \(z\) tweemaal gereduceerd wordt.
Maar stel dat \(z\) reduceren een oneindig proces is, zoals bijvoorbeeld bij \(\skiS\skiI\skiI(\skiS\skiI\skiI)\), dan zal de interpretator bij deze volgorde ook nooit eindigen.
Maar als \(x = \skiK w\) en \(y = \skiK v\), dan is \(z\) niet nodig om het resultaat te bepalen, en kan een andere volgorde het programma wel uitvoeren.
De SKI-interpretator geschreven voor dit onderzoek reduceert altijd de meest linker waarde, een methode die nooit in een oneindige recursie terecht blijkt te komen als dat niet nodig is (\cite[p. 41]{Turner79}).

De reductie van \(\skiS\) is al gegeven in figuur \ref{fig:ski:skiSbegin} en \ref{fig:ski:skiSend}.
In figuur \ref{fig:ski:skiIKbegin} en \ref{fig:ski:skiIKend} is weergegeven hoe \(\skiI\) en \(\skiK\) worden gereduceerd.
De reden waarom \(\skiK xy\) tot \(\skiI x\) wordt gereduceerd is dat het werk anders niet meer gedeeld wordt.
Figuur \ref{fig:ski:skiYbegin} en \ref{fig:ski:skiYend} laten zien hoe \(\skiY\) gereduceerd wordt.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
    app/.style={rectangle split, rectangle split horizontal, rectangle split parts=2, thick, draw=black},
  ]
  \node[app] (app1) {};
    \draw[fill] ($(app1.east)!0.5!(app1.text split)$) circle (0.03);
    \draw[fill] ($(app1.west)!0.5!(app1.text split)$) circle (0.03);

    \node[knoop] (I) [below left=of app1] {\(\skiI\)};
    \draw[->] ($(app1.west)!0.5!(app1.text split)$) -- (I.north east);

    \node[knoop] (x1) [below right=of app1] {\(x\)};
    \draw[->] ($(app1.east)!0.5!(app1.text split)$) -- (x1.north west);

    % graaf 2
    \node[app] (app3) [right=5cm of app1] {};
    \draw[fill] ($(app3.east)!0.5!(app3.text split)$) circle (0.03);
    \draw[fill] ($(app3.west)!0.5!(app3.text split)$) circle (0.03);
    \node[app] (app4) [below left=of app3] {};
    \draw[fill] ($(app4.east)!0.5!(app4.text split)$) circle (0.03);
    \draw[fill] ($(app4.west)!0.5!(app4.text split)$) circle (0.03);
    \draw[->] ($(app3.west)!0.5!(app3.text split)$) -- (app4.north);

    \node[knoop] (K) [below left=of app4] {\(\skiK\)};
    \draw[->] ($(app4.west)!0.5!(app4.text split)$) -- (K.north east);

    \node[knoop] (x) [below right=of app4] {\(x\)};
    \draw[->] ($(app4.east)!0.5!(app4.text split)$) -- (x.north west);

    \node[knoop] (y) [below right=of app3] {\(y\)};
    \draw[->] ($(app3.east)!0.5!(app3.text split)$) -- (y.north west);
  \end{tikzpicture}
  \caption{Grafen van \(\skiI x === x\) en \(\skiK xy === \skiI x === x\)}
  \label{fig:ski:skiIKbegin}
\end{figure}
\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
    app/.style={rectangle split, rectangle split horizontal, rectangle split parts=2, thick, draw=black},
  ]
    \node[knoop] (x1) [left=of I]{\(x\)};
    \node[app] (app1) {};
    \draw[fill] ($(app1.east)!0.5!(app1.text split)$) circle (0.03);
    \draw[fill] ($(app1.west)!0.5!(app1.text split)$) circle (0.03);

    \node[knoop] (I) [below left=of app1] {\(\skiI\)};
    \draw[->] ($(app1.west)!0.5!(app1.text split)$) -- (I.north east);

    \node[knoop] (x1) [below right=of app1] {\(x\)};
    \draw[->] ($(app1.east)!0.5!(app1.text split)$) -- (x1.north west);
  \end{tikzpicture}
  \caption{Gereduceerde grafen van \(\skiI x === x\) en \(\skiK xy === \skiI x === x\) na \'e\'en reductiestap}
  \label{fig:ski:skiIKend}
\end{figure}
\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
    app/.style={rectangle split, rectangle split horizontal, rectangle split parts=2, thick, draw=black},
  ]
    \node[app] (app1) {};
    \draw[fill] ($(app1.east)!0.5!(app1.text split)$) circle (0.03);
    \draw[fill] ($(app1.west)!0.5!(app1.text split)$) circle (0.03);

    \node[knoop] (Y) [below left=of app1] {\(\skiY\)};
    \draw[->] ($(app1.west)!0.5!(app1.text split)$) -- (Y.north east);

    \node[knoop] (f) [below right=of app1] {\(f\)};
    \draw[->] ($(app1.east)!0.5!(app1.text split)$) -- (f.north west);
  \end{tikzpicture}
  \caption{Graaf van \(\skiY f === f (\skiY f)\)}
  \label{fig:ski:skiYbegin}
\end{figure}
\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
    app/.style={rectangle split, rectangle split horizontal, rectangle split parts=2, thick, draw=black},
  ]
    \node[app] (app2) {};
    \draw[fill] ($(app2.east)!0.5!(app2.text split)$) circle (0.03);
    \draw[fill] ($(app2.west)!0.5!(app2.text split)$) circle (0.03);

    \node[app] (app1) [below right=of app2]{};
    \draw[fill] ($(app1.east)!0.5!(app1.text split)$) circle (0.03);
    \draw[fill] ($(app1.west)!0.5!(app1.text split)$) circle (0.03);
    \draw[->] ($(app2.east)!0.5!(app2.text split)$) -- (app1.north);

    \node[knoop] (Y) [below left=of app1] {\(\skiY\)};
    \draw[->] ($(app1.west)!0.5!(app1.text split)$) -- (Y.north east);

    \node[knoop] (f) [below=3cm of app1] {\(f\)};
    \draw[->] ($(app1.east)!0.5!(app1.text split)$) -- (f.north);
    \draw[->] ($(app2.west)!0.5!(app2.text split)$) to [out=230,in=180] (f.west);
    % \draw[->] ($(app2.west)!0.5!(app2.text split)$) to [bend right] (x.west);
  \end{tikzpicture}
  \caption{Gereduceerde graaf van \(\skiY f === f (\skiY f)\) na \'e\'en reductiestap}
  \label{fig:ski:skiYend}
\end{figure}

Naast de combinatoren zijn er ook getallen en functies.
Alle functies hebben twee getallen als argumenten, en geven ofwel een getal ofwel een booleaanse waarde terug.
Een getal kan natuurlijk niet gereduceerd worden maar een functie-applicatie wel.
Omdat functies concrete getallen nodig hebben worden de \(x\) en de \(y\) in \(\lamApp{f}{x}{y}\), waarin \(f\) een functie is, eerst tot getallen gereduceerd;
als dit geen getallen blijken te zijn, stopt de interpretator met een fout.
Hierna wordt de functie gereduceerd zoals te zien is in figuur \ref{fig:ski:funbegin} en \ref{fig:ski:funend}.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
    app/.style={rectangle split, rectangle split horizontal, rectangle split parts=2, thick, draw=black},
  ]
    \node[app] (app1) {};
    \draw[fill] ($(app1.east)!0.5!(app1.text split)$) circle (0.03);
    \draw[fill] ($(app1.west)!0.5!(app1.text split)$) circle (0.03);

    \node[app] (app2) [below left=of app1] {};
    \draw[fill] ($(app2.east)!0.5!(app2.text split)$) circle (0.03);
    \draw[fill] ($(app2.west)!0.5!(app2.text split)$) circle (0.03);
    \draw[->] ($(app1.west)!0.5!(app1.text split)$) -- (app2.north);

    \node[knoop] (fun) [below left=of app2] {\(f\)};
    \draw[->] ($(app2.west)!0.5!(app2.text split)$) -- (fun.north east);

    \node[knoop] (x) [below right=of app2] {\(n_1\)};
    \draw[->] ($(app2.east)!0.5!(app2.text split)$) -- (x.north west);

    \node[knoop] (y) [below right=of app1] {\(n_2\)};
    \draw[->] ($(app1.east)!0.5!(app1.text split)$) -- (y.north west);
  \end{tikzpicture}
  \caption{Graaf van \(f\ n_1\ n_2 === r\)}
  \label{fig:ski:funbegin}
\end{figure}
\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={circle, draw=black, fill=white, thick, minimum size=5mm},
    app/.style={rectangle split, rectangle split horizontal, rectangle split parts=2, thick, draw=black},
  ]
    \node[knoop] (r) {\(r\)};
  \end{tikzpicture}
  \caption{Gereduceerde graaf van \(f\ n_1\ n_2 === r\)}
  \label{fig:ski:funend}
\end{figure}

Hier kan \(r\) een getal zijn of een booleaanse waarde, afhankelijk van de functie. Zie \S\ref{sec:theoretische-inleiding:ski} voor booleaanse waarden in de SKI-calculus.

\subsection{Semantiek}
Hieronder zijn de evaluatieregels gegeven die de interpretator hanteert.
Merk op dat er gebruik wordt gemaakt van zowel \emph{bigstep}- (\(\Downarrow\)) als \emph{smallstep}-semantiek (\(\rightarrow\)) (\cite[\S 3.4]{Pierce02}).
Niet voor alle functies is een formele definitie gegeven, omdat de evaluatie hetzelfde is, op enkele triviale aanpassingen na.
De variabelen \(t\) worden gebruikt voor termen, \(f\) voor functies, \(n\) voor getallen.
De premisse \(t \Downarrow n\) houdt bijvoorbeeld in dat de waarde van \(t\) een getal is, als de term helemaal gereduceerd is.

\begin{equation*}
  \inference[\textsc{E-I}: ]{
      }{
      \lamApp{\skiI}{t} \rightarrow t
  }
\end{equation*}

\begin{equation*}
  \inference[\textsc{E-K}: ]{
      }{
      \lamApp{\skiK}{t_1}{t_2} \rightarrow \lamApp{\skiI}{t_1}
  }
\end{equation*}

\begin{equation*}
  \inference[\textsc{E-S}: ]{
      }{
  \lamApp{\skiS}{t_1}{t_2}{t_3} \rightarrow \lamApp{t_1}{t_3}{(\lamApp{t_2}{t_3})}
  }
\end{equation*}

\begin{equation*}
  \inference[\textsc{E-Y}: ]{
      }{
      \lamApp{\skiY}{t} \rightarrow \lamApp{t}{(\lamApp{\skiY}{t})}
  }
\end{equation*}

\begin{equation*}
  \inference[\textsc{E-Fun}: ]{
      t_1 \Downarrow n_1 & t_2 \Downarrow n_2
  }{
  \lamApp{f}{t_1}{t_2} \rightarrow \lamApp{f}{n_1}{n_2}
  }
\end{equation*}

\newcommand{\inferFun}[3]{
\begin{equation*}
    \inference[\textsc{#1}: ]{
        % #2 & n_1 & n_2
    }{
        \lamApp{#2}{n_1}{n_2} #3
    }
\end{equation*}
}

% Besloten om niet alle functies te doen maar slechts voorbeelden
\inferFun{E-\(+\)}{\skifn{add}}{\Downarrow n_1 + n_2}
% \inferFun{Sub}{\skifn{sub}}{n_1 - n_2}
% \inferFun{Mul}{\skifn{mul}}{n_1 * n_2}
% \inferFun{Mod}{\skifn{mod}}{n_1 \% n_2}
\inferFun{E-\(=\)}{\skifn{eq}}{
    \rightarrow
    \begin{cases}
        \skiT \quad (=== \skiK), & \text{als } n_1 = n_2 \\
        \skiF \quad (=== \skiS\skiK), & \text{anders} \\
    \end{cases} \\
}
