\section{SSML naar SKI}\label{sec:ssml-naar-ski}
In dit hoofdstuk is de vertaling van SSML naar SKI beschreven.
In \S\ref{sec:ssml-naar-ski:vertaalproces} wordt het algehele vertaalproces besproken.
In \S\ref{sec:ssml-naar-ski:vertaling} wordt specifiek de vertaling van SSML- naar SKI-termen beschreven.

\subsection{Het vertaalproces}\label{sec:ssml-naar-ski:vertaalproces}
Voor het hele proces zijn twee programma's geschreven: een compiler die SSML inleest en naar SKI vertaalt en een interpretator die de SKI kan uitvoeren.
De compiler is geschreven in Haskell, de interpretator in Rust.
Beide programma's zijn beschikbaar via \url{https://gitlab.com/splintah/pws}.

Het vertaalproces verloopt zoals is weergegeven in figuur \ref{fig:uitvoerschema-ssml}.
De groene delen worden uitgevoerd door de compiler (hoofdstuk \ref{sec:ssml}), de blauwe door de interpretator (hoofdstuk \ref{sec:ski}).
Nadat de compiler de invoer heeft ontleed, wordt de typecheck uitgevoerd.
Deze geeft als resultaat ofwel dat de invoer correct getypeerd is, ofwel dat de invoer incorrect getypeerd is met een foutmelding.
Als de typecheck succesvol verlopen is, wordt de invoer vertaald naar SKI-combinatoren.
Het SKI-programma wordt in een bestand opgeslagen.
De SKI-interpretator kan dat bestand dan ontleden en uitvoeren, waarna er een resultaat getoond wordt.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[
    knoop/.style={rectangle, draw=black, fill=white, thick, minimum size=5mm},
    haskell/.style={fill=green!20},
    rust/.style={fill=blue!20},
  ]

    \node[knoop] (invoer) {Invoer};
    \node[knoop, haskell] (syntaxisboom) [below=of invoer] {Abstractesyntaxisboom};
    \node[knoop, haskell] (typeschema) [below=of syntaxisboom] {Ja of nee};
    \node[knoop] (ski) [right=of typeschema] {SKI-combinatoren};
    \node[knoop, rust] (ski-rust) [below=of ski] {SKI-graaf};
    \node[knoop] (end) [below=of ski-rust] {Resultaat};

    \draw[->,green!70!black] (invoer.south) -- (syntaxisboom.north) node[left, midway] {Ontleden};
    \draw[->,green!70!black] (syntaxisboom.south) -- (typeschema.north) node[left, midway] {Typecheck};
    \draw[->,green!70!black] (syntaxisboom.south) -- (ski.north) node[right=5mm, midway] {Vertaling};
    \draw[->,blue!70!black] (ski.south) -- (ski-rust.north) node[left, midway] {Ontleden};
    \draw[->,blue!70!black] (ski-rust.south) -- (end.north) node[left, midway] {Interpreteren};

  \end{tikzpicture}
  \caption{Het uitvoerschema van SSML-programma's}
  \label{fig:uitvoerschema-ssml}
\end{figure}

\subsection{De vertaling}\label{sec:ssml-naar-ski:vertaling}
In deze paragraaf worden de vertaalschema's van SKI-expressies naar SKI-termen gedefinieerd.
Het schema \(\scheme[\ \Gamma\ ]{P}{p}\) geeft de vertaling van programma's \(p\) aan, \(\scheme[\ \Gamma\ ]{T}{t}\) voor termen \(t\) en \(\scheme{O}{\mathit{op}}\) voor binaire operaties \(\mathit{op}\).
\(\Gamma\) is een verzameling paren \(x : \sigma\), waar \(x\) een variabele is en \(\sigma\) een SKI-term.
Deze omgeving is nodig voor het vertalen van programma's en termen, omdat de declaraties in programma's variabelen kunnen definiëren, die beschikbaar moeten zijn bij het vertalen van termen.
Zie \cite[\S 5.2]{Dijkstra01} voor verdere informatie over vertaalschema's.

Het vertaalschema voor programma's is als volgt.
Wanneer het einde van het programma bereikt is (\(P = \emptyset\)), dan wordt de functie \(\lamvar{main}\) gecompileerd.
De functie \(\lamvar{main}\) moet bestaan voordat het programma gecompileerd wordt; de typechecker controleert daarom of \(\lamvar{main}\) bestaat (zie \S\ref{sec:ssml:typering}).
Als er nog declaraties zijn (\(P = \{ x = t_1 \} \cup P'\)), dan wordt \(P'\) gecompileerd met een omgeving waaraan het resultaat van de vertaling van \(t_1\) is toegevoegd onder de naam \(x\).
Als \(x\) recursief gedefinieerd is (dan geldt \(x \in FV(t_1)\)), dan wordt gebruik gemaakt van een \textbf{let}-\textbf{rec}-expressie:
\begin{align*}
  \scheme[\ \Gamma\ ]{P}{\emptyset} & === \scheme[\ \Gamma\ ]{T}{\lamvar{main}} \\
  \scheme[\ \Gamma\ ]{P}{\{ x = t_1 \} \cup P'} & === \scheme[\ (\Gamma \cup \{x : t_1'\})\ ]{P}{P'} \\*
    & \quad\quad \text{waar } t_1' =
      \begin{cases}
        \scheme[\ \Gamma\ ]{T}{\lamLetRec{x}{t_1}{x}}, & \text{als } x \in FV(t_1) \\
        \scheme[\ \Gamma\ ]{T}{t_1}, & \text{anders }
      \end{cases}
\end{align*}

Het vertaalschema voor termen is als volgt.
De vertaling van \(\lambda\)-abstracties is gebaseerd op \cite{Lynn}:

\begin{align*}
  \scheme[\ \Gamma\ ]{T}{x} & ===
    \begin{cases}
      \sigma, & \text{als } x : \sigma \in \Gamma \\
      x, & \text{anders} \\
    \end{cases} \\
  \scheme[\ \Gamma\ ]{T}{\lamTrue} & === \skiT \quad (=== \skiK) \\
  \scheme[\ \Gamma\ ]{T}{\lamFalse} & === \skiF \quad (=== \skiS \skiK) \\
  \scheme[\ \Gamma\ ]{T}{n} & === n, \quad \text{als } n \in \mathbb{N} \\
  \scheme[\ \Gamma\ ]{T}{\lamApp{t_1}{t_2}} & === \lamApp{(\scheme[\ \Gamma\ ]{T}{t_1})}{(\scheme[\ \Gamma\ ]{T}{t_2})} \\
  \scheme[\ \Gamma\ ]{T}{\lamLet{x}{t_1}{t_2}} & === \scheme[\ \Gamma\ ]{T}{\lamApp{(\ssmlAbs{x}{t_2})}{t_1}} \\
  \scheme[\ \Gamma\ ]{T}{\lamLetRec{x}{t_1}{t_2}} & === \scheme[\ \Gamma\ ]{T}{\lamLet{x}{\lamFix{(\ssmlAbs{x}{t_1})}}{t_2}} \\
  \scheme[\ \Gamma\ ]{T}{\lamIfThenElse{t_1}{t_2}{t_3}} & === \lamApp{(\scheme[\ \Gamma\ ]{T}{t_1})}{(\scheme[\ \Gamma\ ]{T}{t_2})}{(\scheme[\ \Gamma\ ]{T}{t_3})} \\
  \scheme[\ \Gamma\ ]{T}{\lamFix{t_1}} & === \lamApp{\skiY}{(\scheme[\ \Gamma\ ]{T}{t_1})} \\
  \scheme[\ \Gamma\ ]{T}{t_1\ \mathit{op}\ t_2} & === \lamApp{(\scheme{O}{\mathit{op}})}{(\scheme[\ \Gamma\ ]{T}{t_1})}{(\scheme[\ \Gamma\ ]{T}{t_2})} \\
  \scheme[\ \Gamma\ ]{T}{\ssmlAbs{x}{t_1}} & ===
    \begin{cases}
      \skiI, & \text{als } t_1' = x \\
      \lamApp{\skiK}{t_1'}, & \text{als } x \not\in FV(t_1') \\
      \lamApp{\skiS}{(\scheme[\ \Gamma\ ]{T}{\ssmlAbs{x}{M}})}{(\scheme[\ \Gamma\ ]{T}{\ssmlAbs{x}{N}})}, & \text{als } t_1' = \lamApp{M}{N} \\
    \end{cases} \\*
    & \quad\quad \text{waar } t_1' = \scheme[\ \Gamma\ ]{T}{t_1} \\
\end{align*}

Voor binaire bewerkingen is het vertaalschema als volgt.
Hierbij is geen omgeving \(\Gamma\) nodig:
\begin{align*}
  \scheme{O}{+} & === \lamvar{add}  & \scheme{O}{||} & === \skiT       \\
  \scheme{O}{-} & === \lamvar{sub}  & \scheme{O}{\&\&} & === \skiF     \\
  \scheme{O}{*} & === \lamvar{mul}  & \scheme{O}{<} & === \lamvar{lt}  \\
  \scheme{O}{/} & === \lamvar{div}  & \scheme{O}{<=} & === \lamvar{le} \\
  \scheme{O}{\%} & === \lamvar{mod} & \scheme{O}{>} & === \lamvar{gt}  \\
                &                   & \scheme{O}{>=} & === \lamvar{ge}
\end{align*}