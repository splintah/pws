# Een compiler voor SSML

Deze map bevat het profielwerkstuk van Sybrand Aarnoutse en Splinter Suidman.
Hier zijn het werkstuk, de presentatie en de implementaties van de programma's beschreven in het werkstuk te vinden.
Zie https://splintah.gitlab.io/pws voor meer informatie.
