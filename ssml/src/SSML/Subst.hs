{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}

module SSML.Subst
  ( HasFreeVars (..)
  , Subst (..)
  , emptySubst
  , Substitutable (..)
  , occursIn
  ) where

import           Data.List       (intercalate)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Set        (Set)
import qualified Data.Set        as Set

import Text.Pretty

import SSML.Syntax.Name

-- | 'Subst' @s@ is een substitutie in een of ander type @a@ (zie
-- 'Substitutable'), dat waarden van type @s@ bevat, waarin de waarden @s@
-- vervangen worden, geassocieerd door 'Name'.
newtype Subst s = Subst { getSubst :: Map Name s }
  deriving (Show, Eq)

-- | 'emptySubst' is de lege substitutie. Met @OverloadedLists@ kan dit, naar
-- smaak, ook geschreven worden als 'Subst' @[]@.
emptySubst :: Subst s
emptySubst = Subst []

instance (Pretty s) => Pretty (Subst s) where
  pretty (Subst []) = "{}"
  pretty (Subst s) = "{ " <> substs <> " }"
    where
      substs
        = intercalate ", "
        . fmap (\(x, t) -> pretty x <> " => " <> pretty t)
        $ Map.toList s

instance Functor Subst where
  fmap f = Subst . fmap f . getSubst

-- | 'HasFreeVars' @a@ is een typeklasse voor types @a@ die vrije variabelen van
-- het type 'Name' bevatten.
class HasFreeVars a where
  -- | Geef een verzameling van de vrije variabelen in @a@ terug.
  freeVars :: a -> Set Name

-- | 'occursIn' @x@ @a@ geeft terug of @x@ voorkomt in de vrije variabelen van
-- @a@.
occursIn :: (HasFreeVars a) => Name -> a -> Bool
occursIn x a = x `Set.member` freeVars a

instance (Foldable f, HasFreeVars a) => HasFreeVars (f a) where
  freeVars = foldMap freeVars

-- | 'Substitutabble' @a@ @s@ is een type @a@, dat waarden van type @s@ bevat,
-- waarvoor gesubstitueerd kan worden, geassocieerd door 'Name'.
class Substitutable a s where
  apply :: Subst s -> a -> a

instance (Substitutable s s) => Substitutable (Subst s) s where
  apply (Subst s1) (Subst s2) = Subst $ fmap (apply (Subst s1)) s2 <> s1

instance (Substitutable a s) => Substitutable [a] s where
  apply s = fmap (apply s)

instance (Substitutable a s) => Substitutable (Map k a) s where
  apply s = fmap (apply s)

instance (Substitutable a s, Ord a) => Substitutable (Set a) s where
  apply s = foldMap (singleton . apply s)
    where
      singleton :: Ord a => a -> Set a
      singleton x = [x]

instance (Substitutable s s) => Semigroup (Subst s) where
  (<>) = apply

instance (Substitutable s s) => Monoid (Subst s) where
  mempty = Subst []
