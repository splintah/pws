module SSML.Syntax.Program
  ( Program (..)
  ) where

import Text.Pretty

import SSML.Syntax.Decl

newtype Program = Program { unProgram :: [Decl] }
  deriving (Show, Eq, Semigroup, Monoid)

instance Pretty Program where
  pretty = unlines . fmap pretty . unProgram
