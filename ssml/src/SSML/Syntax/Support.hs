module SSML.Syntax.Support
  ( applyIf
  , wrap
  , parenthesise
  ) where

applyIf :: Bool -> (a -> a) -> a -> a
applyIf p f = if p then f else id

wrap :: String -> String -> String -> String
wrap open close s = open <> s <> close

parenthesise :: String -> String
parenthesise = wrap "(" ")"
