module SSML.Syntax.Type
  ( Type (..)
  , tBool
  , tInt
  ) where

import           Data.List       (intercalate)
import qualified Data.Map.Strict as Map
import qualified Data.Set        as Set
import           Data.String

import Text.Pretty

import SSML.Subst
import SSML.Syntax.Name
import SSML.Syntax.Support

data Type
  -- | Een typevariabele. In Haskell aangegeven met een naam die begint met een
  -- kleine letter.
  = TVar Name
  -- | Een constant type. In Haskell aangegeven met een naam die begint met een
  -- hoofdletter.
  | TCon Name
  -- | 'TArr' @a@ @b@ is het type van een functie van @a@ naar @b@.
  | TArr Type Type
  -- | Producttype: 'TProduct' @a@ @b@ is een paar van @a@ en @b@, geschreven @a
  -- * b@.
  | TProduct Type Type
  --- -- | Somtype: 'TSum' @a@ @b@ is een waarde van ofwel @a@, ofwel @b@,
  --- -- geschreven @a + b@.
  deriving (Show, Eq, Ord)

tBool :: Type
tBool = TCon "Bool"

tInt :: Type
tInt = TCon "Int"

instance Pretty Type where
  pretty t = case t of
    TVar n -> pretty n
    TCon c -> pretty c
    TArr t1 t2 ->
      let t1' = applyIf (precedence t1 <= p) parenthesise . pretty $ t1
          t2' = applyIf (precedence t2 <  p) parenthesise . pretty $ t2
       in t1' <> " -> " <> t2'
    TProduct t1 t2 ->
      let t1' = applyIf (precedence t1 <  p) parenthesise . pretty $ t1
          t2' = applyIf (precedence t2 <= p) parenthesise . pretty $ t2
       in t1' <> " * " <> t2'
    where
      p = precedence t
      precedence = \case
        TVar {} -> 2
        TCon {} -> 2
        TProduct {} -> 1
        TArr {} -> 0

instance IsString Type where
  fromString = TVar . fromString

instance HasFreeVars Type where
  freeVars = \case
    TVar x         -> [x]
    TCon c         -> []
    t1 `TArr` t2   -> freeVars t1 <> freeVars t2
    TProduct t1 t2 -> freeVars t1 <> freeVars t2

instance Substitutable Type Type where
  apply s@(Subst subst) t = case t of
    TVar x         -> Map.findWithDefault t x subst
    TCon c         -> TCon c
    t1 `TArr` t2   -> apply s t1 `TArr` apply s t2
    TProduct t1 t2 -> TProduct (apply s t1) (apply s t2)
