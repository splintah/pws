module SSML.Syntax.Scheme
  ( Scheme (..)
  ) where

import qualified Data.Map.Strict as Map
import qualified Data.Set        as Set

import Text.Pretty

import SSML.Subst
import SSML.Syntax.Name
import SSML.Syntax.Type

data Scheme = Forall [Name] Type
  deriving (Show, Eq, Ord)

instance Pretty Scheme where
  pretty (Forall [] t) = pretty t
  pretty (Forall as t) =
    let as' = unwords . fmap pretty $ as
     in "forall " <> as' <> ". " <> pretty t

instance HasFreeVars Scheme where
  freeVars (Forall as t) = freeVars t `Set.difference` Set.fromList as

instance Substitutable Scheme Type where
  apply (Subst subst) (Forall as t) =
    let subst' = Subst $ foldr Map.delete subst as
     in Forall as (apply subst' t)
