module SSML.Syntax.Decl
  ( Decl (..)
  ) where

import Text.Pretty

import SSML.Syntax.Expr
import SSML.Syntax.Name

data Decl
  = Decl Name Expr
  deriving (Show, Eq)

instance Pretty Decl where
  pretty = \case
    Decl x e -> pretty x <> " = " <> pretty e
