module SSML.Compiler
  ( module Exports
  , Compiler
  , runCompiler
  , compileProgram
  , compileDecl
  , compileExpr
  , Abstractable (..)
  ) where

import SSML.Compiler.SKI as Exports

import Polysemy
import Polysemy.Error
import Polysemy.Reader

import           SSML.Env    as Env
import qualified SSML.Error  as Err
import           SSML.Subst
import           SSML.Syntax

type SKIEnv = Env SKI

type Compiler = Sem '[Reader SKIEnv, Error Err.Error]

runCompiler :: SKIEnv -> Compiler a -> Either Err.Error a
runCompiler env = run . runError . runReader env

compileProgram :: Program -> Compiler SKI
-- Als de typechecker is geslaagd, zou er een main-functie moeten bestaan.
compileProgram (Program [])     = compileExpr "main"
compileProgram (Program (s:ss)) = compileDecl s (compileProgram (Program ss))

compileDecl :: Decl -> Compiler a -> Compiler a
compileDecl decl@(Decl x e) m = do
  e' <- compileExpr $
    -- Gebruik ELetRec als x recursief gedefinieerd is (dan komt x vrij voor in
    -- e). Gebruik anders de e zelf, zodat de uitvoer geen Y-combinator hoeft te
    -- gebruiken.
    if x `occursIn` e
      then ELetRec x e (EVar x)
      else e
  local (insert (x, e')) m

compileExpr :: Expr -> Compiler SKI
compileExpr = abstract
-- TODO: abstract kan ook een fout maken, als niet alle variabelen worden
-- verwijderd. Moet dit gecheckt worden, of gaan we ervan uit dat dit niet
-- gebeurt?

class Abstractable a where
  abstract :: a -> Compiler SKI

instance Abstractable Expr where
  abstract expr = flip catch (throw . Err.InExpr expr) $ do
    case expr of
      EVar x -> do
        env <- ask
        case Env.lookup x env of
          Just ski -> pure ski
          Nothing  -> pure (SKIVar x)
      ELit (EInt i) -> pure (SKIInt i)
      ELit (EBool b) -> pure $ if b then skiT else skiF
      EApp e1 e2 -> SKIApp <$> abstract e1 <*> abstract e2
      ELet x e1 e2 -> abstract ((EAbs x e2) `EApp` e1)
      ELetRec x e1 e2 -> abstract (ELet x (EFix (EAbs x e1)) e2)
      EIf e1 e2 e3 -> (\e1' e2' e3' -> e1' `SKIApp` e2' `SKIApp` e3')
        <$> abstract e1
        <*> abstract e2
        <*> abstract e3
      EProduct {} -> undefined
      EFix e1 -> (Y `SKIApp`) <$> abstract e1
      EBinOp b e1 e2 -> (\b' e1' e2' -> b' `SKIApp` e1' `SKIApp` e2')
        <$> abstract (SKIBinOp b)
        <*> abstract e1
        <*> abstract e2
      EAbs x e1 -> do
        e1' <- abstract e1
        case e1' of
          SKIVar y | x == y -> pure I
          _ | not (x `occursIn` e1') -> pure $ K `SKIApp` e1'
          SKIApp m n -> (\m' n' -> S `SKIApp` m' `SKIApp` n')
            <$> abstract (SKIAbs x m)
            <*> abstract (SKIAbs x n)
          SKIAbs {} -> error "Dit zou onmogelijk moeten zijn, gezien abstract SKIAbs weghaalt."

instance Abstractable SKI where
  abstract = \case
    S -> pure S
    K -> pure K
    I -> pure I
    Y -> pure Y
    SKIVar x -> do
      env <- ask
      case Env.lookup x env of
        Just ski -> pure ski
        Nothing  -> pure (SKIVar x)
    SKIInt i -> pure (SKIInt i)
    SKIApp e1 e2 -> SKIApp <$> abstract e1 <*> abstract e2
    SKIAbs x e1 -> do
      e1' <- abstract e1
      case e1' of
        SKIVar y | x == y -> pure $ I
        _ | not (x `occursIn` e1') -> pure $ K `SKIApp` e1'
        SKIApp m n -> (\m' n' -> S `SKIApp` m' `SKIApp` n')
          <$> abstract (SKIAbs x m)
          <*> abstract (SKIAbs x n)
        SKIAbs {} -> error "Dit zou onmogelijk moeten zijn, gezien abstract SKIAbs weghaalt."
    SKIBinOp EAnd -> pure skiF
    SKIBinOp EOr -> pure skiT
    SKIBinOp b -> pure (SKIBinOp b)
