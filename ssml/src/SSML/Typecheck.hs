module SSML.Typecheck
  ( Typecheck
  , runTypecheck
  , checkProgram
  , checkDecl
  , schemeOf
  , module Exports
  ) where

import SSML.Typecheck.Fresh as Exports
import SSML.Typecheck.Infer as Exports
import SSML.Typecheck.Unify as Exports

import Polysemy
import Polysemy.Error
import Polysemy.Reader

import Data.Maybe

import           SSML.Env    as Env
import qualified SSML.Error  as Err
import           SSML.Subst
import           SSML.Syntax

type Typecheck = Sem '[Reader TypeEnv, Error Err.Error]

runTypecheck :: TypeEnv -> Typecheck a -> Either Err.Error a
runTypecheck env = run . runError . runReader env

checkProgram :: Program -> Typecheck ()
checkProgram (Program []) = do
  env <- ask @TypeEnv
  if isJust $ Env.lookup "main" env
    then pure ()
    else throw Err.NoMainFunction
checkProgram (Program (s:ss)) = checkDecl s (checkProgram (Program ss))

checkDecl :: Decl -> Typecheck a -> Typecheck a
checkDecl decl@(Decl x e) m = do
  -- TODO: waarschuwing voor variableschaduwen.
  s <- flip catch (throw . Err.InDecl decl) . schemeOf $ ELetRec x e (EVar x)
  local (insert (x, s)) m

schemeOf :: Expr -> Typecheck Scheme
schemeOf expr = do
  env <- ask
  (cs, _fs, ty) <- either throw pure $ runInfer env newInferState (infer expr)
  subst <- either throw pure . runSolve $ solve cs
  let ty' = apply subst ty
  let s = simplScheme env . generalise env $ ty'
  pure s
