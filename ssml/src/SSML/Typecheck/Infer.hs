{-# LANGUAGE FlexibleContexts #-}

module SSML.Typecheck.Infer
  ( infer
  , Infer
  , runInfer
  , TypeEnv (..)
  , newTypeEnv
  , InferState (..)
  , newInferState
  , instantiate
  , generalise
  , simplScheme
  ) where

import Control.Lens

import Polysemy
import Polysemy.Error
import Polysemy.Reader
import Polysemy.State
import Polysemy.Writer

import qualified Data.Infinite   as Inf
import           Data.List       (intercalate)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Set        (Set)
import qualified Data.Set        as Set

import Text.Pretty

import           SSML.Env             as Env
import qualified SSML.Error           as Err
import           SSML.Subst
import           SSML.Syntax
import           SSML.Typecheck.Fresh
import           SSML.Typecheck.Unify

type Infer = Sem '[State InferState, Writer [Constraint], Reader TypeEnv, Error Err.Error]

runInfer :: TypeEnv -> InferState -> Infer a -> Either Err.Error ([Constraint], InferState, a)
runInfer env state
  = fmap (\(a, (b, c)) -> (a, b, c))
  . run
  . runError
  . runReader env
  . runWriter
  . runLazyState state

type TypeEnv = Env Scheme

newTypeEnv :: TypeEnv
newTypeEnv = Env []

type InferState = Fresh

newInferState :: InferState
newInferState = defaultFresh

fresh :: Infer Type
fresh = do
  (t, f) <- gets getFreshTVar
  put f
  pure t

instantiate :: Scheme -> Infer Type
instantiate (Forall as t) = do
  as' <- mapM (const fresh) as
  let subst = Subst . Map.fromList . zip as $ as'
  pure $ apply subst t

generalise :: TypeEnv -> Type -> Scheme
generalise env t =
  let as = Set.toList (freeVars t `Set.difference` freeVars env)
   in Forall as t

inEnv :: (Name, Scheme) -> Infer a -> Infer a
inEnv binding = local (Env.insert binding)

lookupEnv :: Name -> Infer Type
lookupEnv x = do
  env <- ask
  case Env.lookup x env of
    Nothing -> throw $ Err.WithEnv env $ Err.UnboundVar x
    Just s  -> instantiate s

constraint :: Member (Writer [Constraint]) r => Type -> Type -> Sem r ()
constraint t1 t2 = tell @[Constraint] [t1 :~: t2]

constraintExpr :: Member (Writer [Constraint]) r => Type -> Type -> Expr -> Sem r ()
constraintExpr t1 t2 expr = tell @[Constraint] [ConstraintExpr (t1 :~: t2) expr]

infer :: Expr -> Infer Type
infer expr = flip catch (throw . Err.InExpr expr) $
  case expr of
    ELit EBool {} -> pure tBool
    ELit EInt {} -> pure tInt

    EVar x -> lookupEnv x

    EAbs x e1 -> do
      f <- fresh
      t1 <- inEnv (x, Forall [] f) $ infer e1
      pure $ f `TArr` t1

    EApp e1 e2 -> do
      t1 <- infer e1
      t2 <- infer e2
      f <- fresh
      constraintExpr t1 (t2 `TArr` f) expr
      pure f

    ELet x e1 e2 -> do
      env <- ask
      (cs, t1) <- listen $ infer e1
      subst <- solve cs
      let t1' = apply subst t1
          s   = generalise (apply subst env) t1'
      inEnv (x, s) $ infer e2

    ELetRec x e1 e2 ->
      infer $ ELet x (EFix (EAbs x e1)) e2

    EIf e1 e2 e3 -> do
      t1 <- infer e1
      t2 <- infer e2
      t3 <- infer e3
      constraintExpr t1 tBool expr
      constraintExpr t2 t3 expr
      pure t2

    EProduct e1 e2 -> do
      t1 <- infer e1
      t2 <- infer e2
      pure $ TProduct t1 t2

    EFix e1 -> do
      -- NOOT: fix : forall a. (a -> a) -> a
      t1 <- infer e1
      f <- fresh
      constraintExpr (f `TArr` f) t1 expr
      pure f

    EBinOp op e1 e2 -> case op of
      EAdd -> inferBinOp tInt tInt e1 e2
      ESub -> inferBinOp tInt tInt e1 e2
      EMul -> inferBinOp tInt tInt e1 e2
      EDiv -> inferBinOp tInt tInt e1 e2
      EMod -> inferBinOp tInt tInt e1 e2
      EAnd -> inferBinOp tBool tBool e1 e2
      EOr  -> inferBinOp tBool tBool e1 e2
      ELT  -> inferBinOp tInt tBool e1 e2
      ELE  -> inferBinOp tInt tBool e1 e2
      EGT  -> inferBinOp tInt tBool e1 e2
      EGE  -> inferBinOp tInt tBool e1 e2

  where
    -- | 'inferBinOp' @tArg@ @tRes@ @e1@ @e2@ voert 'infer' uit voor een binaire
    -- bewerking op @e1@ en @e2@ met argumenten van type @tArg@ en resultaat van
    -- type @tRes@.
    inferBinOp tArg tRes e1 e2 = do
      t1 <- infer e1
      t2 <- infer e2
      constraintExpr t1 tArg expr
      constraintExpr t2 tArg expr
      pure tRes

simplScheme :: TypeEnv -> Scheme -> Scheme
simplScheme env (Forall as t) =
  let as'
        = Inf.take (length as)
        . Inf.filter (not . (`occursIn` env))
        $ unFresh defaultFresh
      subst = Subst . Map.fromList . zip as . fmap TVar $ as'
   in Forall as' (apply subst t)
