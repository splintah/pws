module SSML.Typecheck.Fresh
  ( Fresh (..)
  , defaultFresh
  , getFreshName
  , getFreshTVar
  ) where

import Data.Infinite as Inf
import Data.String

import SSML.Syntax

newtype Fresh = Fresh { unFresh :: Infinite Name }
  deriving (Show, Eq)

defaultFresh :: Fresh
defaultFresh = Fresh
  . Inf.concatMap (\n -> [fromString (l : n) | l <- ['a'..'z']])
  . ("" :::)
  . fmap show
  $ infiniteWith 1 succ

getFreshName :: Fresh -> (Name, Fresh)
getFreshName (Fresh (n:::ns)) = (n, Fresh ns)

getFreshTVar :: Fresh -> (Type, Fresh)
getFreshTVar = (\(n, ns) -> (TVar n, ns)) . getFreshName
