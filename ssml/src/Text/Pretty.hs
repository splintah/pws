module Text.Pretty
  ( Pretty (..)
  , putPretty
  , putPrettyLn
  ) where

class Pretty a where
  pretty :: a -> String

-- | 'putPretty' @=@ 'putStr' '.' 'pretty'.
putPretty :: (Pretty a) => a -> IO ()
putPretty = putStr . pretty

-- | 'putPretty' @=@ 'putStrLn' '.' 'pretty'.
putPrettyLn :: (Pretty a) => a -> IO ()
putPrettyLn = putStrLn . pretty

instance Pretty Int where
  pretty = show

instance Pretty Integer where
  pretty = show

instance Pretty Float where
  pretty = show

instance Pretty Double where
  pretty = show

instance Pretty Bool where
  pretty = show
