{ mkDerivation, base, Comb, containers, haskeline, lens, polysemy
, stdenv, text
}:
mkDerivation {
  pname = "ssml";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base Comb containers haskeline lens polysemy text
  ];
  executableHaskellDepends = [ base Comb haskeline lens polysemy ];
  license = stdenv.lib.licenses.agpl3;
}
