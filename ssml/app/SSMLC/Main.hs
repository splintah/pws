module Main where

import Comb
import Data.String
import Data.Traversable   (for)
import SSML
import System.Environment
import Text.Pretty

main :: IO ()
main = do
  args <- getArgs
  filenames <- case args of
    []        -> ioError . userError $ "Usage: ssmlc [input file]."
    filenames -> pure filenames

  for filenames $ \filename -> do
    putStrLn $ "ssmlc: reading input file ‘" <> filename <> "’."
    file <- readFile filename

    putStrLn "ssmlc: parsing input file."
    let res = getResult $ runParser parseProgram (fromString file)
    program <- case res of
      Left _       -> ioError . userError $ "Parse error."
      Right (p, _) -> pure p

    putStrLn "ssmlc: typechecking program."
    typecheck program

    putStrLn "ssmlc: compiling program."
    ski <- compile program

    putStrLn $ "ssmlc: writing to file ‘" <> filename <> ".ski’."
    writeFile (filename <> ".ski") (pretty ski)

  putStrLn "ssmlc: succes!"

typecheck :: Program -> IO ()
typecheck program = do
  let res = runTypecheck (Env []) (checkProgram program)
  case res of
    Left err -> do
      ioError . userError $ "Error while typechecking program:\n" <> pretty err
    Right () -> pure ()

compile :: Program -> IO SKI
compile program = do
  let res = runCompiler (Env []) (compileProgram program)
  case res of
    Left err -> do
      ioError . userError $ "Error while compiling program:\n" <> pretty err
    Right ski -> pure ski
