{ nixpkgs ? import <nixpkgs> {}, compiler ? "ghc865" }:
let haskell = nixpkgs.pkgs.haskell.packages.${compiler}.override {
    overrides = self: super: {
      Comb =
        let src = nixpkgs.fetchFromGitLab {
              owner = "splintah";
              repo = "Comb";
              rev = "b39654028711d4cda0fe55aabf3014d49698831d";
              sha256 = "0db9gpqkdlcjr9y87kdv26zs6lbg615indk4iv5cnp4az3vcpxrj";
            };
        in self.callPackage src {};
    };
  };
in haskell.callPackage ./ssml.nix { }
