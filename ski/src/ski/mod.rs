mod builtin;
mod combinator;
mod element;

pub use builtin::{Function, Value};
pub use combinator::Combinator;
pub use element::Element;

pub struct Program {
    instructions: Element,
}

impl std::fmt::Display for Program {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.instructions)?;

        Ok(())
    }
}

impl Program {
    pub fn new(instructions: Element) -> Program {
        Program { instructions }
    }

    pub fn execute(mut self) -> Element {
        let mut stack = Vec::new();
        
        unsafe {
            self.instructions.reduce(&mut stack);
        }

        stack.pop().unwrap()
    }
}

#[test]
fn test_succ_zero() {
    let text = "add(1)(0)";
    let program = super::parse::parse_program(text).unwrap();
    println!("{}", program);

    let res = program.execute();

    assert_eq!(res, 1.into());

    // println!("{:?}", example);
}

#[test]
fn test_y_combinator() {
    let text = "
    (SI(K8))
(Y(
    S
    (S(KS)(
        S
        (S(KS)(S(S(KS)(S(S(KS)(S(KK)
            (Keq)
            ))(KI)))(S(KK)(K0))))
        (S(KK)(K6))
    ))
    (S(S(KS)(S(KK)I))(S(S(KS)(S(S(KS)(S(KK)(Ksub)))(KI)))(S(KK)(K1))))
))";
    let program = super::parse::parse_program(text).unwrap();
    let res = program.execute();

    assert_eq!(res, 6.into());
}
