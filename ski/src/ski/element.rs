use super::{Combinator, Value};

#[derive(Clone, Debug)]
pub struct Element(*mut Combinator);

impl std::fmt::Display for Element {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        unsafe { write!(f, "{}", self.borrow()) }
    }
}

impl<T: Into<Combinator>> From<T> for Element {
    fn from(a: T) -> Element {
        Element::new(a.into())
    }
}

impl PartialEq<Element> for Element {
    fn eq(&self, rhs: &Element) -> bool {
        unsafe { self.borrow() == rhs.borrow() }
    }
}

impl Element {
    pub fn new(n: Combinator) -> Element {
        let box_ptr = Box::new(n);

        // Leak memory here!
        let ptr: *mut Combinator = Box::leak(box_ptr);
        Element(ptr)
    }

    pub fn rc_clone(&self) -> Element {
        Element(self.0)
    }

    pub unsafe fn rc_clone_right(&self) -> Element {
        let tmp = self.borrow();
        match &*tmp {
            Combinator::Application(_, r) => r.rc_clone(),
            _ => panic!(),
        }
    }

    pub unsafe fn borrow_mut(&self) -> &mut Combinator {
        &mut *self.0
    }

    pub unsafe fn borrow(&self) -> &Combinator {
        &*self.0
    }

    pub unsafe fn reduce(&mut self, application_stack: &mut Vec<Element>) {
        use Combinator::*;

        match self.borrow() {
            Application(fst, _snd) => {
                // Push self onto the stack.
                application_stack.push(self.rc_clone());

                // Go further down the left side
                application_stack.push(fst.rc_clone());
            }
            K => {
                let self_app = application_stack.pop().unwrap();
                let x = self_app.rc_clone_right();

                let up_app = application_stack.last().unwrap();
                *up_app.borrow_mut() = Application(I.into(), x);
            }
            I => {
                let self_app = application_stack.last_mut().unwrap();
                let x = self_app.rc_clone_right();
                *self_app = x;
            }
            S => {
                let self_app = application_stack.pop().unwrap();
                let up_app = application_stack.pop().unwrap();
                let up2_app = application_stack.last().unwrap();

                let f = self_app.rc_clone_right();
                let g = up_app.rc_clone_right();
                let x = up2_app.rc_clone_right();

                let f_x = Application(f, x.rc_clone()).into();
                let g_x = Application(g, x).into();

                let tmp = up2_app.borrow_mut();
                let tmp2 = tmp.unwrap_app();
                *tmp2.0 = f_x;
                *tmp2.1 = g_x;
            }
            Y => {
                let self_app = application_stack.last_mut().unwrap();
                let tmp = self_app.borrow_mut();
                let tmp2 = tmp.unwrap_app();
                *tmp2.0 = tmp2.1.rc_clone();
                let new = Application(Y.into(), tmp2.1.rc_clone()).into();
                *tmp2.1 = new;
            }
            Other(val) => {
                if let Value::Function(f) = val {
                    let res = f.exec(application_stack);

                    let top = application_stack
                        .last_mut()
                        .expect("No top value for function evaluation.");
                    *top.borrow_mut() = Application(I.into(), res);
                } else {
                    unimplemented!()
                }
            }
        }

        if !is_done(application_stack) {
            let mut next = application_stack.pop().unwrap();
            // println!("Next: {}", next);
            next.reduce(application_stack);
        }
        //  else {
        //     for i in application_stack {
        //         print!("{} ", i);
        //     }
        //     print!("\n");
        // }
    }

    pub unsafe fn reduce_to_other(&mut self) -> Value {
        match self.borrow() {
            Combinator::Other(v) => return v.clone(),
            Combinator::Application(..) => {}
            error => panic!("Expected Combinator::Other, got {}.", error),
        }

        let mut stack = Vec::new();
        self.reduce(&mut stack);
        stack.pop().unwrap().reduce_to_other()
    }
}

unsafe fn is_done(stack: &mut Vec<Element>) -> bool {
    if stack.len() == 1 {
        match stack.last().unwrap().borrow() {
            Combinator::Application(..) => false,
            _ => true,
        }
    } else {
        stack.len() <= 1
    }
}
