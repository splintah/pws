use super::{Element, Function, Value};

#[derive(Clone, Debug, PartialEq)]
pub enum Combinator {
    S,
    K,
    I,
    Y,
    Application(Element, Element),
    Other(Value),
}

impl From<Value> for Combinator {
    fn from(v: Value) -> Combinator {
        Combinator::Other(v)
    }
}

impl From<Function> for Combinator {
    fn from(v: Function) -> Combinator {
        Value::Function(v).into()
    }
}

impl From<bool> for Combinator {
    fn from(b: bool) -> Combinator {
        use Combinator::*;

        if b {
            K
        } else {
            Combinator::app(S, K)
        }
    }
}

impl From<i64> for Combinator {
    fn from(n: i64) -> Combinator {
        Combinator::Other(Value::Number(n))
    }
}

impl std::fmt::Display for Combinator {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use Combinator::*;
        let text = match self {
            S => "S".to_string(),
            K => "K".to_string(),
            I => "I".to_string(),
            Y => "Y".to_string(),
            Application(a, b) => format!("({}{})", a, b),
            Other(n) => format!("{}", n),
        };

        write!(f, "{}", text)
    }
}

impl Combinator {
    pub fn app<A: Into<Element>, B: Into<Element>>(a: A, b: B) -> Combinator {
        Combinator::Application(a.into(), b.into())
    }

    pub fn unwrap_app(&mut self) -> (&mut Element, &mut Element) {
        if let Combinator::Application(left, right) = self {
            (left, right)
        } else {
            panic!()
        }
    }
}
